#!/usr/local/bin/perl

use English;
use strict;
use utf8;
use warnings;

use File::Slurp;

use FindBin qw($Bin);
use lib $Bin;

use Cred;
use MilHist::Bot;
use MilHist::Parser;

my $cred   = new Cred ();
my $editor = MilHist::Bot->new ($cred) or
  die 'new MediaWiki::Bot failed';

my $prep_area;
my $prep_page;
my $prep_number;

binmode (STDOUT, ':utf8');
binmode (STDERR, ':utf8');

sub read_prep_area ($) {
    my ($prep_card) = @ARG;
    $prep_card =~ /prep (\d+)/ or
      die "first card should be 'prep #'\n";
    $prep_number = $1;
    $prep_page   = "Template:Did you know/Preparation area $1";
    my $prep_area_text = $editor->fetch ($prep_page);
    $prep_area = new MilHist::Parser ('text' => $prep_area_text);
}

sub add_image ($) {
    my ($nomination) = @ARG;
    my $nom_image  = $nomination->find ('main page image');
    my $prep_image = $prep_area->find  ('main page image');
    foreach my $param ('image', 'caption', 'width') {
        $prep_image->add ($param => $nom_image->get ($param));
    }
}

sub add_make ($) {
    my ($nomination) = @ARG;
    my @make = $nomination->find ('DYKmake');
    die "Not found!\n" unless @make;

    MAKE: foreach my $make (@make) {
        my $done         = 0;
        my @placeholders = $prep_area->find ('DYKmake');
        foreach my $placeholder (@placeholders) {
            my $title = $placeholder->get (1);
            if ($title eq 'Example') {
                $prep_area->replace ($placeholder, $make);
                next MAKE;
            }
        }
        $prep_area->add ($make, $placeholders[-1]);
    }
}

sub get_hook ($) {
    my ($nomination) = @ARG;
    my @hooks = $nomination->text =~ /(('''ALT\d+''':){0,1}\.\.\. that .+\?)/g;
    @hooks = grep { defined } @hooks;

    # For now... main hook only
    return $hooks[0];
}

sub add_hook ($$) {
    my ($nomination, $match) = @ARG;
    my $hook     = get_hook ($nomination);
    my $old_text = $prep_area->find_text ($match);
    my $new_text = $old_text;
    $new_text =~ s/\Q$match\E/$hook/;
    $prep_area->replace ($old_text, $new_text);
}

sub nomination ($$) {
    my ($nomination_article, $sequence_number) = @ARG;
    my $nomination_page = "Template:Did you know nominations/$nomination_article";
    my $nomination_text = $editor->fetch ($nomination_page);
    $nomination_text =~ s/<!--//g;
    $nomination_text =~ s/-->//g;
    my $nomination = new MilHist::Parser ('text' => $nomination_text);

    if (1 == $sequence_number) {
        add_image ($nomination);
        add_hook ($nomination, "... that ... ''(pictured)'' ...");
    } else {
        add_hook ($nomination, '... that ...');
    }
    add_make ($nomination);
}

sub read_nominations (@) {
    my @nominations     = @ARG;
    my $sequence_number = 1;
    foreach my $nomination (@nominations) {
        chomp $nomination;
        nomination ($nomination, $sequence_number++);
    }
}

sub delete_surplus_placeholders () {
    my @placeholders = $prep_area->find ('DYKmake');
    foreach my $placeholder (@placeholders) {
        my $title = $placeholder->get (1);
        if ($title eq 'Example') {
            $prep_area->delete ($placeholder);
        }
    }
}

sub update_prep_area () {
    delete_surplus_placeholders();
    my $prep_text = $prep_area->text;

    #   print $prep_text;

    $editor->edit (
        {
            page    => $prep_page,
            text    => $prep_text,
            summary => "Loaded prep area with hooks",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$prep_page'");
}

sub update_nominations (@) {
    my @nominations = @ARG;
    foreach my $nomination_article (@nominations) {
        chomp $nomination_article;
        my $nomination_page = "Template:Did you know nominations/$nomination_article";
        my $nomination_text = $editor->fetch ($nomination_page);
        my $nomination      = new MilHist::Parser ('text' => $nomination_text);
        my $subpage         = $nomination->find ('DYKsubpage');
        $subpage->display_name ("subst:DYKsubpage\n");
        $subpage->add ('passed' => 'yes');

        #       print $nomination->text;

        $editor->edit (
            {
                page    => $nomination_page,
                text    => $nomination_text,
                summary => "To prep $prep_number",
                minor   => 0,
            }
          ) or
          $editor->error ("unable to edit '$nomination_page'");
    }
}

exit 0;

my $input = shift @ARGV or
    die "usage: autodyk.pl <input file>\n";

my @nominations = read_file ($input);
read_prep_area   (shift @nominations);
read_nominations (@nominations);
update_prep_area();
update_nominations (@nominations);
exit 0;
