#!/usr/bin/perl -w
#
# FAC::Coordinators.pm 
#

package FAC::Coordinators;

use Carp;
use English;
use strict;
use warnings;

sub is_coordinator ($$) {
    my ($self, $user) = @ARG;
    return $self->{'coordinators'}->{ucfirst $user} // 0;    
}

sub coordinators ($) {
    my ($self) = @ARG;
    my $coordinators = $self->{'coordinators'};
	return keys %$coordinators;
}

sub date ($$) {
    my ($self, $coordinator) = @ARG;
    Carp::croak "'$coordinator' is not a coordinator" unless $self->is_coordinator ($coordinator);
    
	if ($self->{'coordinators'}->{$coordinator} eq "date") {
		my $date;
		my $page = $self->{'page'};
		my $editor = $self->{'editor'};
	    my @history = $editor->get_history ($page);
		foreach my $revision (@history) {
			my $text = $editor->get_text ($page, $revision->{revid}) or
				$editor->error ("Unable to find '$page:$revision->{revid}')");
    		if ($text =~ /\Q$coordinator\E/) {
    			$date = $revision->{timestamp_date};
    		}
    	}
    	print "$coordinator appointed on $date\n";
    	$self->{'coordinators'}->{$coordinator} = $date;				
	}
	return $self->{'coordinators'}->{$coordinator};
}


sub DESTROY {
    my $self = shift;
}

sub new {
    my $that  = shift;
    my $class = ref ($that) || $that;
    my $self  = { @ARG };    
    bless $self, $class;
    Carp::croak "editor parameter required by $class" unless $self->{'editor'};

    my $editor = $self->{'editor'};
    $self->{'page'} = 'Template:@FAC';
    my $text = $editor->fetch ($self->{'page'});
    my @coordinators = ( $text =~ /User:(.+?)\|/g );
    my %coordinators = map { $ARG => "date" } @coordinators;
    $self->{'coordinators'} = \%coordinators;
    return $self;
}

1;
