#!/usr/local/bin/perl -w
#
# far.pl -- Pass or fail an Featured Article class review
# 	This Bot runs every day, looking for featured class articles that have been processed by a delegate
#	If it finds one, it follows the steps involved in keeping or delisting it.
# Usage: far.pl
#
# 13 Feb 2015 Created
#  9 Oct 2018 Use Showcase.pm
# 15 Feb 2019 Unescaped left brace is now deprecated

use English;
use strict;
use utf8;
use warnings;

use Carp;
use Data::Dumper;
use File::Basename qw(dirname);
use File::Spec;
use POSIX qw(strftime);
use XML::Simple;

binmode (STDERR, ':utf8');
binmode (STDOUT, ':utf8');

use FindBin qw($Bin);
use lib qw($Bin $Bin/MilHist);

use Cred;
use MilHist::ArticleHistory;
use MilHist::Diff;
use MilHist::Parser;
use MilHist::Showcase;

# Pages used
my $candidates_page = 'Wikipedia:featured article review';
my $showcase_fa     = 'Wikipedia:WikiProject Military history/Showcase/FA';

my $cred = new Cred ();

require MilHist::Bot;
my $editor = MilHist::Bot->new ($cred) or
  die "new MilHist::Bot failed";

sub has_been_closed ($) {
    my ($nomination) = @ARG;
    # $cred->showtime ("\tchecking if $nomination has been closed...\n");
    my $text = $editor->fetch ($nomination);
    if ($text =~ /\{\{FARClosed\|(.+?)\}\}.+(\d+:\d+, \d+ (\w+) (\d+) \(UTC\))/) {
        $cred->showtime ("\t$nomination $1\n");
        return ($1, $2, $3, $4);
    }
    return ();
}

sub whodunnit ($$$) {
    my ($article, $nomination, $action) = @ARG;
    my $old;
    my @history = $editor->get_history ($nomination) or
      $editor->error ("Unable to get history of '$nomination'");
    foreach my $revision (@history) {

        #		print Dumper $revision, "\n";
        my $text = $editor->get_text ($nomination, $revision->{revid}) or
          $editor->error ("Unable to find '$nomination:$revision->{revid}')");
        if ($text !~ /\{\{FARClosed/) {
            $cred->showtime ("\t$article was $action by $old->{user} at $old->{timestamp_date} $old->{timestamp_time}\n");
            my $diff = new MilHist::Diff ('title' => $nomination, 'diff' => $old->{revid}, 'oldid' => $revision->{revid});
            return ($old->{user}, $old->{timestamp_date}, $old->{timestamp_time}, $diff->as_string);
        } else {
            $old = $revision;
        }
    }
}

sub keep_update_nomination_page ($$$$$) {
    my ($page, $nomination, $user, $date, $diff) = @ARG;
    $cred->showtime ("\tUpdating the nomination page\n");
    my $text = $editor->fetch ($nomination);

    # Remove transcluded article links and featured article tools
    $text =~ s/<noinclude>.+<\/noinclude>//s;

    # Tag the top and bottom of the page
    my $result = "'''kept''' by [[User:$user|$user]] via ~~~ $date [$diff]";
    my $top    = "\{\{subst:FAR top|result=$result\}\}";
    my $bottom = "\{\{subst:FAR bottom\}\}\n";
    $text = join "\n", $top, $text, $bottom;

    $editor->edit (
        {
            page    => $nomination,
            text    => $text,
            summary => "'$page' kept",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$nomination'");
}

sub delist_update_nomination_page ($$$$$) {
    my ($page, $nomination, $user, $date, $diff) = @ARG;
    $cred->showtime ("\tUpdating the nomination page\n");
    my $text = $editor->fetch ($nomination);

    # Remove transcluded article links and featured article tools
    $text =~ s/<noinclude>.+<\/noinclude>//s;

    # Tag the top and bottom of the page
    my $result = "'''delisted''' by [[User:$user|$user]] via ~~~ $date [$diff]";
    my $top    = "\{\{subst:FAR top|result=$result\}\}";
    my $bottom = "\{\{subst:FAR bottom\}\}\n";
    $text = join "\n", $top, $text, $bottom;

    $editor->edit (
        {
            page    => $nomination,
            text    => $text,
            summary => "Archiving '$page'",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$nomination'");
}

sub delist_update_article_page ($) {
    my ($page) = @ARG;
    $cred->showtime ("\tUpdating the article page\n");
    my $text = $editor->fetch ($page);
    $text =~ s/\{\{featured article\}\}//igs;

    $editor->edit (
        {
            page    => $page,
            text    => $text,
            summary => "Delisting '$page' after unsuccessful Featured Article Review",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$page'");
}

sub get_revid ($$$) {
    my ($page, $date, $time) = @ARG;
    my @history = $editor->get_history ($page) or
      $editor->error ("Unable to get history of '$page'");
    foreach my $history (@history) {
        if ($history->{timestamp_date} le $date
            || ($history->{timestamp_date} eq $date && $history->{timestamp_time} le $time))
        {
            return $history->{revid};
        }
    }
    $editor->error ("Unable to get revid of '$page')");
}

sub keep_update_talk_page ($$$$$) {
    my ($page, $talk, $nomination_page, $date, $time) = @ARG;

    $cred->showtime ("\tUpdating the talk page\n");
    my $text = $editor->fetch ($talk);

     # Remove the candidacy
    my $parser = new MilHist::Parser ('text' => $text);
    my $far    = $parser->find       ('featured article review');
    $parser->delete ($far);
    $text = $parser->text ();

    # Update the article history
    my $article_history = new MilHist::ArticleHistory (
        'editor' => $editor,
        'parser' => $parser,
        'page'   => $page,
        'talk'   => $talk);
 
    $article_history->merge ();
    $article_history->sort ();
 
    my $oldid = get_revid ($page, $date, $time);
   	$article_history->add_action ('FAR', $date, $nomination_page, 'kept', $oldid);
    $article_history->add ('currentstatus' => 'FA');
    $text = $article_history->text ();
    
    $editor->edit (
        {
            page    => $talk,
            text    => $text,
            summary => "Updating '$page' after successful Featured Article Review",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$talk'");
}

sub delist_update_talk_page ($$$$$) {
    my ($page, $talk, $nomination_page, $date, $time) = @ARG;

    $cred->showtime ("\tUpdating the talk page\n");
    my $text = $editor->fetch ($talk);

    # Remove the candidacy
    my $parser = new MilHist::Parser ('text' => $text);
    my $far    = $parser->find       ('featured article review');
    $parser->delete ($far);
    $text = $parser->text ();
    
    # Update the article history
    my $article_history = new MilHist::ArticleHistory (
        'editor' => $editor,
        'parser' => $parser,
        'page'   => $page,
        'talk'   => $talk);
 
    $article_history->merge ();
    $article_history->sort ();

    my $oldid = get_revid ($page, $date, $time);
   	$article_history->add_action ('FAR', $date, $nomination_page, 'demoted', $oldid);
    $article_history->add ('currentstatus' => 'FFA');
    $text = $article_history->text ();
    $text =~ s/class *=FA/class=/igs;

    $editor->edit (
        {
            page    => $talk,
            text    => $text,
            summary => "Updating '$page' after unsuccessful Featured Article Review",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$talk'");
}

sub remove_from_showcase ($) {
    my ($article) = @ARG;
    my $showcase_text = $editor->fetch ($showcase_fa);
    my $showcase      = new MilHist::Showcase ($showcase_text);
    my $found         = $showcase->del ($article);

    if ($found) {
        $editor->edit (
            {
                page    => $showcase_fa,
                text    => $showcase->text,
                summary => "'$article' has been delisted",
                minor   => 0,
            }
          ) or
          $editor->error ("unable to edit '$showcase_fa'");
    }
    return $found;
}

sub nomination ($$) {
    my ($talk, $template_name) = @ARG;
    my $text = $editor->fetch ($talk);
    my $parser   = new MilHist::Parser ('text' => $text);
    my $template = $parser->find       ($template_name);
    my $path     = $template->get      (1);
    my $nomination = "Wikipedia:$template_name/$path";
    return $nomination;
}

$cred->showtime ("started\n");

my @candidates = $editor->get_pages_in_category ('Wikipedia featured article review candidates');
foreach my $talk (@candidates) {
    my $article = $talk;
    next unless $article =~ s/Talk://;
    eval {
        $cred->showtime ($article, "\n");

        my $nomination = nomination ($talk, 'Featured article review');
        # $cred->showtime ("\t$nomination\n");
        if (my ($status, $display_date, $month, $year) = has_been_closed ($nomination)) {
            $cred->showtime ("\tclosed ($status) on $display_date\n");
            if ($status =~ /kept|keep/i) {
                my ($user, $date, $time, $diff) = whodunnit ($article, $nomination, 'kept');
                keep_update_talk_page ($article, $talk, $nomination, $date, $time);
                keep_update_nomination_page ($article, $nomination, $user, $display_date, $diff);
            } elsif ($status =~ /delisted|removed/i) {
                my ($user, $date, $time, $diff) = whodunnit ($article, $nomination, 'delisted');
                delist_update_talk_page ($article, $talk, $nomination, $date, $time);
                delist_update_nomination_page ($article, $nomination, $user, $display_date, $diff);
                delist_update_article_page    ($article);
                remove_from_showcase          ($article);
            } else {
                $cred->warning ("$nomination has unknown status '$status'\n", $status);
            }
        } else {
            $cred->showtime ("\t$nomination is still current\n");
        }
    };
    if ($EVAL_ERROR) {
        $cred->warning ($EVAL_ERROR);
    }
}
$cred->showtime ("finished\n");
exit 0;
