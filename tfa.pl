#!/usr/local/bin/perl -w
# Handle the recent list in TFAs
#
# 15 Feb 2019 Unescaped left brace is now deprecated

use English;
use strict;
use utf8;
use warnings;

use Carp;
use Data::Dumper;
use DateTime;
use DateTime::Format::Strptime;
use Date::Parse;
use File::Basename qw(dirname);
use File::Spec;
use POSIX;

use FindBin qw($Bin);
use lib qw($Bin $Bin/MilHist);

use Cred;
use MilHist::Bot;

binmode (STDERR, ':utf8');
binmode (STDOUT, ':utf8');

my $cred = new Cred ();

my $editor = MilHist::Bot->new ($cred) or
  die "new MediaWiki::Bot failed";

sub date ($) {
	my ($day) = @ARG;
	my $date = strftime ("%B %d, %Y", gmtime ($day));
	$date =~ s/ 0/ /;  # Get rid of the space
	return $date;
}

my %tfa;
my $today = time ();
my $one_day = 24 * 60 * 60;
my $three_days_ago = $today - 3 * $one_day;
my $sixty_days_time = $today + 60 * $one_day;

#############################################################################################

sub unreviewed_articles () {
	my @unreviewed_pages = ('Wikipedia:Unreviewed featured articles/2020/2004&#8211;2009', 'Wikipedia:Unreviewed featured articles/2020/2010&#8211;2015');
	foreach my $unreviewed_page (@unreviewed_pages) {
		# print "FETCHING $unreviewed_page\n";
		my $unreviewed_text = $editor->fetch ($unreviewed_page);
		my @unreviewed_lines = split /\n/, $unreviewed_text;
		my $unreviewed_updated = 0;
		
		foreach my $date (keys %tfa) {
			my $article =  $tfa{$date}->{article};
			# print "LOOKING for $article ($date)\n";
			if ($unreviewed_text =~ /\Q$article\E/) {
				# print "FOUND article='$article'\n";
				for (my $i = 0; ; ++$i) {
					if ($unreviewed_lines[$i] =~ /\Q$article\E/) {
						last if ($unreviewed_lines[$i] =~ /^#/);
						# print "DATE was $unreviewed_lines[$i-1]\n";
						my $parser = DateTime::Format::Strptime->new(
							pattern  => '%B %d, %Y',
		  					on_error => 'croak',
						);
						my $dt = $parser->parse_datetime ($date);
						my $date2 = $dt->strftime ("%Y-%m-%d");
						# print "DATE NOW $date2\n";
						if ($unreviewed_lines[$i-1] =~ /$date2/) {
							# print "FOUND that date\n";
						} else {
							$cred->showtime ("Updated '$article' to $date2\n");
							$unreviewed_lines[$i-1] = "| $date2";
							$unreviewed_updated++;					
						}			
						last;
					}
				}
			}
		}
			
		if ($unreviewed_updated) {
			$unreviewed_text = join "\n", @unreviewed_lines;

			$editor->edit ({
				page => $unreviewed_page,
				text => $unreviewed_text,
				summary => 'update unreviewed list',
				minor => 0,
			}) or
				$editor->error ("unable to edit '$unreviewed_page'");			
		}
	}
}

#############################################################################################

sub unrequited_articles () {
	my @unreviewed_pages = ('Wikipedia:Unreviewed featured articles/2020/2004&#8211;2009', 'Wikipedia:Unreviewed featured articles/2020/2010&#8211;2015');
	foreach my $unreviewed_page (@unreviewed_pages) {
		my $unreviewed_text = $editor->fetch ($unreviewed_page);
		my @unreviewed_lines = split /\n/, $unreviewed_text;
		my $date1;
		my $date2;
		my @unrequited;
		
		foreach (@unreviewed_lines) {
			if (/^\|-/) {
				$date1 = '';
				
			} elsif (/^\| (\d{4}-\d{2}-\d{2})/) {
				$date1 = $1; 				
				# print "DATE was $date1\n";
				my $parser = DateTime::Format::Strptime->new(
					pattern  => '%Y-%m-%d',
		  			on_error => 'croak',
				);
				my $dt = $parser->parse_datetime ($date1);
				$date2 = $dt->strftime ('%B %d, %Y');
				$date2 =~ s/ 0/ /;
				# print "DATE NOW $date2\n";
				
			} elsif (/(\[\[(?!User:|User talk:|User_talk:|special:).+?\]\])/i) {
				my $article = $1;
				if ($date1 && $tfa{$date2}) {
					# print "FOUND: $article ($date1)\n";
					if ($article eq $tfa{$date2}->{'article'}) {
						#print "MATCHES!\n";
						next;
					} else {
						# print "NO MATCH! -  $date2 article should be '$tfa{$date2}->{article}'\n";
						push @unrequited, $date1;
					}
				}	
			}
		}
			
		if (@unrequited) {
			my $count = 0;
			my @unreviewed_lines_out;
			foreach my $line (@unreviewed_lines) {
				foreach my $date (@unrequited) {
					if ($line =~ /^\|-/) {
						$count = 0;
					} else {
						$count++;
					}
					next unless 2 == $count;
					$line =~ s/$date(?!\])//;
				}
				push @unreviewed_lines_out, $line;
			}
			$unreviewed_text = join "\n", @unreviewed_lines_out;
			
			$editor->edit ({
				page => $unreviewed_page,
				text => $unreviewed_text,
				summary => 'update unrequited list',
				minor => 0,
			}) or
				$editor->error ("unable to edit '$unreviewed_page'");	
		}
	}
}

#############################################################################################

my $prev_date;
for (my $day = $three_days_ago; $day < $sixty_days_time; $day += $one_day) {
	my $date = date ($day);
	my $tfa = "Wikipedia:Today's featured article/$date";

	my $text = $editor->get_text ($tfa) or
		next;

	my $article;
	my $italics = 0;
	foreach ($text) {
        if (/^\{\{Main page image.+?\}\}\s+(\[\[.+?\]\])/) {
            $article = $1;
		} elsif (/'''\{\{lang\|\w+\|(\[\[.+?\]\])\}\}'''/) {
			$article = $1;
			$italics = 1;
		} elsif (/'''''(.+?)'''''/) {
			$article = $1;
			$italics = 1;
		} elsif (/"'''(.+?)'''"/) {
			$article = '"' . $1 . '"';
		} elsif (/'''+(.+?)'''+/) {
			$article = $1;
		}
	}
	if (! defined $article || $article !~ /\[\[/) {
		($article) = $text =~ /(\[\[.+?\]\])/;
	}
	$article =~ s/(\]\])\w+$/$1/;
	$article =~ s/\|.+/]]/;
	$article =~ /(\w)/;
	my $u = uc $1;
	$article =~ s/$1/$u/;
	if ($italics) {
		print "$date article is ''$article''\n";
	} else {
		print "$date article is $article\n";
	}

	$tfa{$date} = { 'article' => $article, 'previous' => $prev_date, 'italics' => $italics};
	$prev_date = $date;
}

unreviewed_articles ();
unrequited_articles ();

eval {
    DAY:foreach my $this_day (keys %tfa) {
    	my $this_day_fa = "Wikipedia:Today's featured article/$this_day";
    	print $this_day, "\n";

    	my $text = $editor->fetch ($this_day_fa);

    	if ($text =~ /TFArecentlist/) {
    		print "\thas recent list...\n";
    	} else {
    		my @previous_tfa;
    		my $previous_day = $this_day;
    		for my $prev (0..2) {
    			$previous_day = $tfa{$previous_day}->{previous};
    			next DAY unless defined $previous_day;
    			print "\tprevious day is $previous_day\n";
    			my $previous_article = $tfa{$previous_day}->{article};
    			if ($tfa{$previous_day}->{italics}) {
    				$previous_article = "''" . $previous_article . "''";
    			}
    			print "\tprevious article is $previous_article\n";
    			push @previous_tfa, $previous_article;
    		}

    		print "\tupdating $this_day...\n";
    		my $list = join "\n", map { "* $ARG" } @previous_tfa;
    		$text =~ s/TFAfooter/TFArecentlist|\n$list\n\}\}\n\n\{\{TFAfooter/;

    #		print $text, "\n";

    		$editor->edit ({
    			page => $this_day_fa,
    			text => $text,
    			summary => 'update recently featured list',
    			minor => 0,
    		}) or
    			$editor->error ("unable to edit '$this_day_fa'");
    	}
    }
};
if ($EVAL_ERROR) {
	$cred->warning ($EVAL_ERROR);
}
$cred->showtime ("finished\n");
exit 0;
