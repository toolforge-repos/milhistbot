#!/usr/local/bin/perl -w
#
# aclass.pl -- Pass or fail an A class review
# 	This Bot runs every hour, looking for A class articles that have been promoted by a MilHist administrator
#	If it finds one, it follows the steps involved in promoting or failing it.
# Usage: aclass.pl
#    30 Mar 13 Add failure option
#     2 Jun 14 Add reassessment options
#     7 Jun 14 Error handling
#    14 Aug 14 Change a message
#     6 Sep 14 Enhancements:
#		(a) Do not stop if a bad nomination is found
#		(b) Follow the redirect is the assessment page is a redirect
#		(c) Mark edits as bot but not minor
#		(d) Make STDERR a utf8
#     8 Sep 14 Some templates are listed with underscores instead of spaces
#		Perform step 4a (removal from nom page) first
# 22 Jan 2016 Do not leave a blank line behind on the review page
#  9 Nov 2016 Patches for nominators in strange format
# 17 Oct 2017 Allow for Coordinators on meta
# 12 Nov 2017 Higher MilHist Awards
# 20 Nov 2017 Higher MilHist Awards
# 22 Feb 2018 MilHist awards script
# 27 Feb 2018 Indent award nom
# 19 Jun 2018 Capitalise Unicode::Collate
# 15 Feb 2019 Unescaped left brace is now deprecated

use English;
use strict;
use utf8;
use warnings;

use Data::Dumper;
use DateTime;
use POSIX;
use Unicode::Collate;

use FindBin qw($Bin);
use lib qw($Bin $Bin/MilHist);

use Cred;
use MilHist::ArticleHistory;
use MilHist::Bot;
use MilHist::Coordinators;
use MilHist::Parser;
use MilHist::Section;
use MilHist::Showcase;
use MilHist::Template;

binmode (STDOUT, ":utf8");
binmode (STDERR, ":utf8");

my $aclass_review    = 'Wikipedia:WikiProject Military history/Assessment/A-Class review';
my $milhist_template = 'Military history|WikiProject Military history|MILHIST|WPMILHIST';

my $dt         = DateTime->now ();
my $year       = $dt->year ();
my $month      = $dt->month ();
my $next       = DateTime->new ('day' => 1, 'month' => $month, 'year' => $year)->add ('months' => 1);
my $next_month = $next->month ();
my $next_year  = $next->year ();

my $Collator = Unicode::Collate->new ();

my $cred   = new Cred ();
my $editor = MilHist::Bot->new ($cred) or
  die "new MediaWiki::Bot failed";
my $coordinators = new MilHist::Coordinators ('editor' => $editor);

sub nominators ($) {
    my ($page) = @ARG;
    my $assessment = $page->{'assessment'};
    my $nom_text = $editor->fetch ($assessment);

    my @nm = ($nom_text =~ /<small>''Nominator\(s\):(.+)/ig);
    my @n1 = $nm[0] =~ /\[\[User:(.+?)[\|\]]/gi;
    my @n2 = $nm[0] =~ /\{\{u\|(.+?)\}\}/gi;
    my @n3 = $nm[0] =~ /\{\{user0\|(.+?)\}\}/gi;
    my @nominators = (@n1, @n2, @n3);

    @nominators or
      $editor->error ("Unable to find nominator");

    # Remove underscores
    @nominators = map { s/_/ /g; $ARG } @nominators;

    return @nominators;
}

sub archive_nomination_page ($) {
    $cred->showtime ("Step1: Archiving the nomination page\n");
    my ($page) = @ARG;
    my $assessment = $page->{'assessment'};
    my $text = $editor->fetch ($assessment);
    my $parser = new MilHist::Parser ('text' => $text);

    my $top     = new MilHist::Template ('name' => 'archive top');
    my $comment = $page->{'comment'};
    $parser->addTop ($top, "\n", "$comment ~~~~\n");

    my $bottom = new MilHist::Template ('name' => 'archive bottom');
    $parser->addBottom ("\n", $bottom, "\n");
    $text = $parser->text ();

    $editor->edit (
        {
            page    => $assessment,
            text    => $text,
            summary => $page->{'summary'},
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$assessment'");
}

sub update_talk_page ($$) {
    my ($result, $page) = @ARG;

    $cred->showtime ("Step2: Get the permanent link\n");
    my $pagename   = $page->{'name'};
    my $talkpage   = $page->{'talk'};
    my $assessment = $page->{'assessment'};
    my $outcome    = $page->{'outcome'};

    my ($history) = $editor->get_history ($pagename) or
		$editor->error ("Unable to get history of '$pagename'");
    my $revision = $history->{'revid'};
    my $date = $history->{'timestamp_date'};

    $cred->showtime ("Step3: Update the talk page\n");
    my $text = $editor->fetch ($talkpage);

    my $parser = new MilHist::Parser ('text' => $text);
    my $article_history = new MilHist::ArticleHistory (
        'editor' => $editor,
        'parser' => $parser,
        'page'   => $pagename,
        'talk'   => $talkpage);
    $article_history->add_action ('WAR', $date, $assessment, $result, $revision);
    
    $article_history->merge ();
    $article_history->sort ();
    
    # Update the current status
    my $currentstatus = $article_history->get ('currentstatus') // '';
    if ($currentstatus eq '') {
	    my @templates = $parser->find_parameter ('class');
	    foreach my $template (@templates) {
	        my $class = $template->get ('class');
	        if ($class eq 'GA') {
	            $article_history->add ('currentstatus' => "GA\n");
	            $currentstatus = 'GA';
	            last;
	        }
	    }
	}

	# Update the project banner
    my @templates = $parser->find ('WikiProject banner shell');
	unless (@templates) {
	    my $match     = $milhist_template . '|WikiProject Ships|WPSHIPS|WikiProject Aviation|ShipwrecksWikiProject';
	   	@templates = $parser->find ($match);
	}

    foreach my $template (@templates) {
        if ($outcome eq 'pass') {
            $template->add ('class' => 'A');
        }
        if ($outcome eq 'demoted') {
        	# Leave for another bot run to re-appraise
            $template->delete ();
        }
    }
    
    $text = $parser->text ();
    #	print $text, "\n";

    $editor->edit (
        {
            page    => $talkpage,
            text    => $text,
            summary => $page->{'summary'},
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$talkpage'");
}

sub update_review_page ($) {
    my ($page) = @ARG;

    $cred->showtime ("Step4a: Update the review page\n");
    my $text = $editor->fetch ($aclass_review);
    $text =~ s/WikiProject_Military_history/WikiProject Military history/g;
    my $parser = new MilHist::Parser ('text' => $text);
    my $p = $page->{'redirect'} // $page->{'assessment'};
    my $template = $parser->find ("\Q$p\E") or
      $editor->error ("Unable to find '$p' on '$aclass_review'");
    $parser->delete ($template);

    $editor->edit (
        {
            page    => $aclass_review,
            text    => $parser->text,
            summary => $page->{'summary'},
            minor   => 0,
        }
      ) or
      $editor->error ("Unable to edit '$aclass_review'");
}

sub assess ($$) {
    my ($year, $status) = @ARG;
    return "[[Wikipedia:WikiProject Military history/Assessment/$year/$status]]\n";
}

sub step4b ($$) {
    $cred->showtime ("Step4b: Archive the review\n");
    my ($result, $page) = @ARG;

    my $text;
    my $archive_page = "Wikipedia:WikiProject Military history/Assessment/$year";
    $text = $editor->get_text ($archive_page) or do {
        my $parser = new MilHist::Parser ('text' => $text);
        my $archive = new MilHist::Template ('name' => 'WPMILHIST Archive');
        $archive->add ('category' => 'review');
        my $navigation = new MilHist::Template ('name' => 'WPMILHIST Navigation');
        $parser->addTop ($archive, "\n", $navigation, " __FORCETOC__\n");
        my $promoted = new MilHist::Section ('name' => 'promoted', 'text' => assess ($year => 'Promoted'));
        my $failed   = new MilHist::Section ('name' => 'failed',   'text' => assess ($year => 'Failed'));
        my $kept     = new MilHist::Section ('name' => 'kept',     'text' => assess ($year => 'Kept'));
        my $demoted  = new MilHist::Section ('name' => 'demoted',  'text' => assess ($year => 'Demoted'));
        my $category = "[[Category:Requests for military history A-Class review| ]]\n";
        $parser->addBottom ($promoted, $failed, $kept, $demoted, $category);
        $text = $parser->text;

        $editor->edit (
            {
                page    => $archive_page,
                text    => $text,
                summary => "Created new archive page for $year",
                minor   => 0,
            }
          ) or
          $editor->error ("Unable to find '$archive'");
    };

    my $category = $result eq 'Promoted' || $result eq 'Kept' ? 'Successful' : 'Failed';
    my $result_page = "Wikipedia:WikiProject Military history/Assessment/$year/$result";
    $text    = $editor->get_text ($result_page) or do {
        $text = join '',
          "== $result ==\n",
          "<!--Please add new reviews directly below this line-->\n",
          "\n",
          "<!--Add archived reviews at the top please-->\n",
          "\n",
          "<noinclude>[[Category:$category requests for military history A-Class review| ]]</noinclude>\n",
          "\n";

        $editor->edit (
            {
                page    => $result_page,
                text    => $text,
                summary => "Created new page for $year/$result",
                minor   => 0,
            }
          ) or
          $editor->error ("Unable to find '$result_page'");
    };

    $cred->showtime ("\tupdating $result_page\n");
    my $assessment = $page->{'assessment'};
    $text =~ s/(<!--Please add new reviews directly below this line-->)/$1\n\{\{$assessment\}\}/s or
      $cred->showtime ("unable to find review section - please fix the comment on the page!\n");

    $editor->edit (
        {
            page    => $result_page,
            text    => $text,
            summary => $page->{'summary'},
            minor   => 0,
        }
      ) or
      $editor->error ("Unable to edit '$result_page'");
}

sub step5 ($) {
    $cred->showtime ("Step5: Update the announcements page\n");
    my ($page) = @ARG;
    my $announcements = "Template:WPMILHIST Announcements";
    my $text = $editor->fetch ($announcements);

    #	print $text, "\n";
    my $pagename   = $page->{'name'};
    my $assessment = $page->{'assessment'};

    my $link = "[[$assessment|$pagename]]";
    $text =~ s/WP:/Wikipedia:/g;
    $text =~ s/(&bull;)* \Q$link\E//s;

    #	print $text, "\n";

    $editor->edit (
        {
            page    => $announcements,
            text    => $text,
            summary => $page->{'summary'},
            minor   => 0,
        }
      ) or
      $editor->error ("Unable to edit '$announcements'");
}

sub step6 ($) {
    $cred->showtime ("Step6: Update the article showcase\n");
    my ($page)     = @ARG;
    my $pagename   = $page->{'name'};
    my $outcome    = $page->{'outcome'};
    my $talkpage   = $page->{'talk'};

    my $talktext   = $editor->fetch ($talkpage);
    my $parser     = new MilHist::Parser ('text' => $talktext);    
    my $milhist    = $parser->find ($milhist_template);
    my $is_list    = $milhist->get ("list") ? 1 : 0;    

    my $showcase_a = 'Wikipedia:WikiProject Military history/Showcase/' . ($is_list ? 'AL' : 'A');    
    my $text       = $editor->fetch ($showcase_a);
    my $showcase   = new MilHist::Showcase ($text);

    if ($outcome eq 'pass') {
        $showcase->add ($pagename);
    }

    if ($outcome eq 'demoted') {
        $showcase->del ($pagename);
    }

    $editor->edit (
        {
            page    => $showcase_a,
            text    => $showcase->text,
            summary => $page->{'summary'},
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$showcase_a'");
}

sub step7 ($) {
    $cred->showtime ("Step7: Update the newsletter\n");
    my ($page) = @ARG;
    my $pagename = $page->{'name'};

    sub nominator_list ($) {
        my ($page) = @ARG;
        my $nominator_list;
        my @nominators = map { "[[User:$ARG|$ARG]]" } nominators ($page);
        if (1 == @nominators) {
            $nominator_list = pop @nominators;
        } else {
            my $last = pop @nominators;
            $nominator_list = join ', ',    @nominators;
            $nominator_list = join ' and ', $nominator_list, $last;
        }
        return $nominator_list;
    }

    my $nominator_list = nominator_list ($page);
    my $new_entry      = "; [[$pagename]] ($nominator_list)\n";

    my $dt            = DateTime->new   (year => $next_year, month => $next_month);
    my $display_month = $dt->month_name ();

    my $next_date  = "$display_month $next_year";
    my $newsletter = "Wikipedia:WikiProject Military history/News/$next_date/Articles";
    my $text       = $editor->get_text ($newsletter);
    if ($text) {
        $cred->showtime ("\tupdating $newsletter\n");

        my @a = split (/^/, $text);
        my @b;
        my $inserting = 0;
        my $inserted  = 0;

        foreach (@a) {
            if (/New A-Class articles/) {
                $inserting = 1;
            }
            if (/Footer/) {
                if (!$inserted) {
                    my $x = pop @b;
                    if ($x !~ /^\s*$/) {
                        push @b, $x;
                    }
                    push @b, $new_entry, "\n";
                }
            }
            if ($inserting) {
                if (/^;\[\[(.+?)\]\]/) {
                    if ($Collator->cmp ($1, $new_entry) > 0) {
                        push @b, "\n", $new_entry;
                        $inserting = 0;
                        $inserted  = 1;
                    }
                }
                if (/^$/) {
                    push @b, "\n", $new_entry;
                    $inserting = 0;
                    $inserted  = 1;
                }
            }
            push @b, $ARG;
        }

        $text = join '', @b;

    } else {
        $cred->showtime ("\tcreating $newsletter\n");

        my $parser = new MilHist::Parser ('text' => '');
        my $header = new MilHist::Template ('name' => "Wikipedia:WikiProject Military history/News/$next_date/Header");
        my @subheadings;
        foreach my $subheading (
            'New featured articles',
            'New featured lists',
            'New featured topics',
            'New featured pictures',
            'New featured portals',
            'New A-Class articles',
          )
        {
            my $subheader = new MilHist::Template ('name' => 'WPMILHIST Newsletter section header 2');
            $subheader->add ('1' => $subheading)->default (1);
            push @subheadings, $subheader->text, "\n\n";
        }
        my $footer = new MilHist::Template ('name' => "Wikipedia:WikiProject Military history/News/$next_date/Footer");
        $parser->addTop ($header->text, ' __NOTOC__', "\n\n", @subheadings, $new_entry, "\n", $footer->text, "\n\n");
        $text = $parser->text;
    }

    $editor->edit (
        {
            page    => $newsletter,
            text    => $text,
            summary => $page->{'summary'},
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$newsletter'");
}

sub coordinator ($) {
    my ($page) = @ARG;
    my $user = $page->{'coordinator'};
    return "[[User:$user|$user]] ([[User talk:$user|talk]])";
}

sub award_tracking ($$) {
    $cred->showtime ("Step8: Tracking award eligibility\n");
    my ($page, $coordinator) = @ARG;

    sub awards ($) {
        my ($nominator) = @ARG;
        my @archives = (
            'Wikipedia talk:WikiProject Military history/Awards',
            'Wikipedia talk:WikiProject Military history/Awards/ACC/Archive 1',
            'Wikipedia talk:WikiProject Military history/Awards/ACR/Archive 2',
            'Wikipedia talk:WikiProject Military history/Awards/ACR/Archive 1'
        );

        foreach my $archive (@archives) {
            my $text  = $editor->fetch ($archive);
            my @lines = reverse split (/^/, $text);
            foreach (@lines) {
                if (/$nominator \((\d+)\)/i) {
                    my $count = $1;
                    $cred->showtime ("$nominator has $count A class medals\n");
                    return $count;
                }
            }
        }
        return 0;
    }

    sub award ($) {
        my ($awards) = @ARG;
        my $award;
        my $articles_per_award;
        my $tracking;
        my $section;
        my $acm_tracking = 'Wikipedia:WikiProject Military history/Awards/ACM/Eligibility tracking';
        my $acc_tracking = 'Wikipedia:WikiProject Military history/Awards/ACC/Eligibility tracking';
        my $acm_section  = 'Nominations for the A-Class Medal';
        my $acc_section  = 'Nominations for the A-Class Cross';

        if ($awards < 35) {
            if ($awards < 5) {
                $award = 'A-Class medal';
            } elsif ($awards < 10) {
                $award = 'A-Class medal with Oak Leaves';
            } elsif ($awards < 20) {
                $award = 'A-Class medal with Swords';
            } else {
                $award = 'A-Class medal with Diamonds';
            }
            $articles_per_award = 3;
            $tracking           = $acm_tracking;
            $section            = $acm_section;
        } else {
            if ($awards < 40) {
                $award = 'A-Class cross';
            } elsif ($awards < 46) {
                $award = 'A-Class cross with Oak Leaves';
            } elsif ($awards < 56) {
                $award = 'A-Class cross with Swords';
            } else {
                $award = 'A-Class cross with Diamonds';
            }
            $articles_per_award = 5;
            $tracking           = $acc_tracking;
            $section            = $acc_section;
        }
        return ($award, $articles_per_award, $tracking, $section);
    }

    sub nomination ($$$$$$@) {
        my ($page_name, $coordinator, $nominee, $award, $awards, $section_name, @pages) = @ARG;
        my $nomination_page = 'Wikipedia_talk:WikiProject_Military_history/Awards';
        my $text            = $editor->fetch ($nomination_page);
        my $parser          = new MilHist::Parser ('text' => $text);
        my $section         = $parser->section ($section_name);

        my $pages = join ', ', map { "[[$ARG]]" } @pages;
        my $nomination_template = new MilHist::Template ('name' => 'WPMILHIST Award nomination');
        $nomination_template->add ('nominee'  => $nominee);
        $nomination_template->add ('citation' => "$pages, and [[$page_name]]");
        $nomination_template->add ('award'    => $award);
        $nomination_template->add ('status'   => 'nominated');

        my $nomination_text = join "\n", $nomination_template->text, "*'''Support''' As nominator $coordinator via ~~~~", "\n";

        my $subsection = new MilHist::Section (
            'name'  => "$nominee ($awards)",
            'text'  => $nomination_text,
            'level' => 4,
        );
        $section->add ($subsection);
        $text = $parser->text;

        $cred->showtime ("\tupdating nomination page $nomination_page\n");
        $editor->edit (
            {
                page    => $nomination_page,
                text    => $text,
                summary => "$nominee ($awards)",
                minor   => 0,
            }
          ) or
          $editor->error ("unable to edit $nomination_page");
    }

    # Find the nominators on the tracking pages
    my $pagename   = $page->{'name'};
    my @nominators = nominators ($page);
    foreach my $nominator (@nominators) {
        my $awards = awards ($nominator) + 1;
        my ($award, $articles_per_award, $tracking, $section_name) = award ($awards);
        my $text = $editor->fetch ($tracking);

        foreach ($text) {
            if (/# '''$nominator:*'''(.*)/) {
                $cred->showtime ("\tFound nominator $nominator in tracking list\n");
                my $string = $1;
                my $count  = 0;
                my @pages;
                while ($string =~ /\[\[File:Symbol a class.svg\|15px\|link=(.+?)\]\]/g) {
                    $count++;
                    push @pages, $1;
                }
                $cred->showtime ("\tfound $count nominations for $nominator\n");
                ++$count;

                if ($count < $articles_per_award) {
                    s/(# '''$nominator:*'''.*)/$1 [[File:Symbol a class.svg|15px|link=$pagename]]/;
                } else {
                    s/(# '''$nominator:*''')(.+)/$1/;
                    nomination ($page->{'name'}, $coordinator, $nominator, $award, $awards, $section_name, @pages);
                }
            } else {
                $cred->showtime ("\tCould not find nominator $nominator in tracking list -- adding\n");
                my @a = split (/^/, $text);
                my @b;
                my $found    = 0;
                my $inserted = 0;
                foreach (@a) {
                    if (!$inserted) {
                        if (/^# '''(.+):*'''/) {
                            $found++;
                            if ($1 gt $nominator) {
                                push @b, "# '''$nominator:''' [[File:Symbol a class.svg|15px|link=$pagename]]\n";
                                $inserted = 1;
                            }
                        }
                        if (/^$/ && $found) {
                            push @b, "# '''$nominator:''' [[File:Symbol a class.svg|15px|link=$pagename]]\n";
                            $inserted = 1;
                        }
                    }
                    push @b, $ARG;
                }
                $text = join '', @b;
            }

            $cred->showtime ("\tupdating tracking page $tracking\n");
            $editor->edit (
                {
                    page    => $tracking,
                    text    => $text,
                    summary => $page->{'summary'},
                    minor   => 0,
                }
              ) or
              $editor->error ("unable to edit $tracking");
        }
    }
}

sub pass ($) {
    my ($page) = @ARG;
    my $pagename = $page->{'name'};
    $cred->showtime ("Passing $pagename\n");
    my $coordinator = coordinator ($page);
    $page->{'comment'} = "Article '''promoted''' by $coordinator via";
    $page->{'summary'} = "$pagename Passed A class review";

    update_review_page      ($page);
    archive_nomination_page ($page);
    update_talk_page        ('approved', $page);
    step4b                  ('Promoted', $page);
    step5                   ($page);
    step6                   ($page);
    step7                   ($page);
    award_tracking          ($page, $coordinator);
    $cred->showtime ("done\n");
}

sub failed ($) {
    my ($page) = @ARG;
    my $pagename = $page->{'name'};
    $cred->showtime ("Failing $pagename\n");
    my $coordinator = coordinator ($page);
    $page->{'comment'} = "'''No consensus to promote''' at this time - $coordinator via";
    $page->{'summary'} = "$pagename failed A class review";

    update_review_page      ($page);
    archive_nomination_page ($page);
    update_talk_page        ('not approved', $page);
    step4b                  ('Failed', $page);
    step5                   ($page);
    $cred->showtime ("done\n");
}

sub kept ($) {
    my ($page) = @ARG;
    my $pagename = $page->{'name'};
    $cred->showtime ("Keeping $pagename\n");
    my $coordinator = coordinator ($page);
    $page->{'comment'} = "Article still meets A-Class criteria  - $coordinator via";
    $page->{'summary'} = "$pagename kept after A class reappaisal";

    update_review_page      ($page);
    archive_nomination_page ($page);
    update_talk_page        ('kept', $page);
    step4b                  ('Kept', $page);
    step5                   ($page);
    $cred->showtime ("done\n");
}

sub demoted ($) {
    my ($page) = @ARG;
    my $pagename = $page->{'name'};
    $cred->showtime ("Demoting $pagename\n");
    my $coordinator = coordinator ($page);
    $page->{'comment'} = "Article no longer meets A-Class criteria - $coordinator via";
    $page->{'summary'} = "$pagename demoted after A class reappaisal";

    update_review_page      ($page);
    archive_nomination_page ($page);
    update_talk_page        ('demoted', $page);
    step4b                  ('Demoted', $page);
    step5                   ($page);
    step6                   ($page);
    $cred->showtime ("done\n");
}

sub whodunnit ($) {
    my ($page) = @ARG;
    my $user;
    my $outcome  = $page->{'outcome'};
    my $talkpage = $page->{'talk'};
    my @history  = $editor->get_history ($talkpage) or
      $editor->error ("unable to get history of $talkpage'");
    foreach my $history (@history) {
        my $text = $editor->get_text ($talkpage, $history->{revid}) or
          $editor->error ("unable to find '$talkpage' revid $history->{revid}");
        my $parser   = new MilHist::Parser ('text' => $text);
        my $template = $parser->find  ($milhist_template);
        last unless ($template);
        my $status = lc ($template->get ('A-Class') // 'unknown');

        #	    print "revid $history->{revid} user='$history->{user}' status='$status'\n";
        last if ($outcome ne $status);
        $user = $history->{user};
    }

    #	print "user=", $user, "\n";
    return $user;
}

sub outcome ($) {
    my ($page) = @ARG;
    while (1) {
        my $talkpage = $page->{'talk'};
        my $text     = $editor->fetch ($talkpage);
        my $parser   = new MilHist::Parser ('text' => $text);
        if ($parser->isRedirect ()) {
            $page->{'talk'} = $parser->redirect ();
            $cred->showtime ("'$talkpage' redirects to '$page->{'talk'}'\n");
            redo;
        }
        my $template = $parser->find ($milhist_template) or
          die "Could not find MilHist template on '$talkpage'";
        my $outcome = lc ($template->get ('A-Class') // 'unknown');
        $page->{'outcome'} = $outcome;
        return $outcome;
    }
}

sub find_page ($) {

    # set the globals
    my $page = {};
    my ($assessment) = @ARG;
    while (1) {
        $assessment =~ s/_/ /g;
        $assessment =~ /Wikipedia:WikiProject Military history\/Assessment\/(.+)/;
        my $pagename = $1;
        my $talkpage = "Talk:$pagename";
        my $redirect;

        my $text = $editor->fetch ($assessment);
        my $parser = new MilHist::Parser ('text' => $text);
        if ($parser->isRedirect ()) {
            ($redirect, $assessment) = ($assessment, $parser->redirect ());
            $cred->showtime ("'$redirect' redirects to '$assessment'\n");
            redo;
        }

        #   print $pagename, "\n";
        $page->{'name'}       = $pagename;
        $page->{'talk'}       = $talkpage;
        $page->{'assessment'} = $assessment;
        $page->{'redirect'}   = $redirect;
        last;
    }
    return $page;
}

sub find_pages () {
    my $text = $editor->fetch ($aclass_review);
    my $parser    = new MilHist::Parser ('text' => $text);
    my $match     = 'Wikipedia:WikiProject Military history\/Assessment\/(?!ACR\/)(.+)';
    my @templates = $parser->find ($match);
    my @pages     = map { find_page ($ARG->name ()); } @templates;
    return @pages;
}

$cred->showtime ("begin\n");
eval {
    my @pages = find_pages();
    foreach my $page (@pages) {

        my $pagename = $page->{'name'};
        $cred->showtime ("Checking $pagename...\n");
        my $outcome = outcome ($page);
        $cred->showtime ($outcome, "\n");

        if ($outcome eq 'current') {
            next;
        } elsif ($outcome eq 'unknown') {
            next;
        } else {
            my $user = whodunnit ($page);
            if ($coordinators->is_coordinator ($user)) {
                $cred->showtime ("$user is a ccoordinator\n");
            } else {
                $cred->warning ("$user is NOT a ccoordinator\n");
                next;
            }

            $page->{'coordinator'} = $user;
            if ($outcome eq 'pass' || $outcome eq 'passed') {
                pass ($page);
            } elsif ($outcome eq 'fail' || $outcome eq 'failed') {
                failed ($page);
            } elsif ($outcome eq 'kept') {
                kept ($page);
            } elsif ($outcome eq 'demoted') {
                demoted ($page);
            } else {
                $cred->warning ("unknown outcome: '$outcome'\n");
            }
        }
    }
};
if ($EVAL_ERROR) {
    $cred->error ($EVAL_ERROR);
}
$cred->showtime ("finished\n");
exit 0;
