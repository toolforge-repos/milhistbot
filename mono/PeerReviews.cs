using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wikimedia
{
	public class PeerReviews
	{
		private PeerReviews()
		{
			const string category = "Old requests for peer review";
			Bot bot = new Bot();
			var query = new Query(category, 100);
			int count = 1;

			do
			{
				var pages = bot.Category(query);
				foreach (var page in pages)
				{
					page.Load();

					var templates = page.Templates.FindAll(ind => ind is OldPeerReview);
					foreach (var template in templates)
					{
						if (null != page.ArticleHistory)
						{
							// Console.WriteLine (page.Title);
							page.ArticleHistory.Add(template);
							page.ArticleHistory.Sort();
							page.Remove(template);
							page.Save("Merge old peer review into article history");

							Template diff = new Template("Diff");
							diff.Add("title", page.Title);
							diff.Add("oldid", page.OldRevId.ToString());
							diff.Add("diff",  page.RevId.ToString());
							diff.Add("label", count.ToString());
							Console.WriteLine(diff);
							count++;
						}
					}
				}
			}
			while (query.Continue);
		}
	}
}
