using System;
using System.Collections.Generic;
using NDesk.Options;

public class AutoClass
{
    private int  max     = 1;
    private bool debugs  = false;
    private bool force   = false;
    private bool help    = false;
    private bool verbose = false;


	private void writeLine (string line)
	{
		if (debugs || verbose)
		{
			Console.WriteLine (line);		
		}	
	}
	
    private void autoClass (Page talk)
    {
    	try
    	{
	        Page article = talk.Article;
	        Console.WriteLine (article.Title);
	        if (article.IsRedirect)
	        {
	        	article = article.RedirectsTo.Article;
				writeLine ("    Redirects to " + article.Title);
	        }
	        
	        var taskForces = new TaskForces (article);
	        if (taskForces.Any ())
	        {		
				talk.Load ();
		        talk.MilHist.ProjectTemplate.Add (taskForces);
		        writeLine ("   Added " + taskForces);
		        if (force)
		        {
		        	if (article.IsDisambiguation)
		        	{
		        		writeLine ("   Disambiguation page");
		        		talk.MilHist.ProjectTemplate.Class = "Disambig";	
		        	}
		        	else if (article.IsProject)
		        	{
		        		writeLine ("   Project page");
		        		talk.MilHist.ProjectTemplate.Class = "Project";	
		        	}
		        	else if (talk.IsRedirect)
		        	{
		        		writeLine ("   Redirect");
		        		talk.MilHist.ProjectTemplate.Class = "Redirect";	
		        	}
		        	talk.Save ("AutoClass: Added task forces");
		        }
		    }
	        else
	        {
	            Console.Error.WriteLine (article.Title + ": No task forces found");
	        }
	    }
	    catch (PageMissingException pmex)
	    {
	    	talk.Bot.Cred.Warning (pmex.Message);
	    }
    }

    private static void showHelp (OptionSet optionSet)
    {
        Console.WriteLine ("Usage: mono AutoClass [OPTIONS]+ <article>");
        Console.WriteLine ("Add task forces to the MilHist template on the talk page.");
        Console.WriteLine ();
        Console.WriteLine ("Options:");
        optionSet.WriteOptionDescriptions (Console.Out);
        Environment.Exit (0);
    }

    List<string> options (string [] args)
    {
        var optionSet = new OptionSet () {
            { "d|debug",   "debugging",    v => debugs  = v != null },
            { "f|force",   "update page",  v => force   = v != null },
            { "h|?|help",  "display help", v => help    = v != null },
            { "n|max=",    "number of pages to process",  v => int.TryParse (v, out max) },
            { "v|verbose", "vebosity",     v => verbose = v != null },
        };

        List<string> extras = optionSet.Parse (args);

        if (help) 
        {
            showHelp (optionSet);
        }
  
        return extras;
    }

    private AutoClass (string [] args)
    {
        var bot = new Bot ();
        var articles = options (args); 
        Debug.Bot = bot;
        Debug.On  = debugs;

        bot.Cred.Showtime ("started");
        if (articles.Count > 0)
        {
            var articlePage = new Page (bot, articles[0]);
            autoClass (articlePage.Talk);
        }
        else
        {
            var query = new Query ("Military history articles with no associated task force", max);
            var talkPages = bot.Category (query);
        
            foreach (var talkPage in talkPages) 
            {
                if (talkPage.Namespace.Equals("Talk"))
                { 
                    autoClass (talkPage);
                }
            }
        }
        bot.Cred.Showtime ("done");
    }

    static public void Main (string [] args)
    {
        new AutoClass (args);
    }
}
