using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using NDesk.Options;

public class AutoReport
{
    private const string coordinatorsPageTitle = "Wikipedia talk:WikiProject Military history/Coordinators";
    private const string comment = "Automatic MILHIST checklist assessment - B class";

	private Bot bot;    
    private bool debugs  = false;
    private bool force   = false;
    private bool help    = false;
    private bool verbose = false;


    private static void showHelp (OptionSet options)
    {
        Console.WriteLine ("Usage: mono AutoReport [OPTIONS]+");
        Console.WriteLine ("Report on articles assessed as B class last month.");
        Console.WriteLine ();
        Console.WriteLine ("Options:");
        options.WriteOptionDescriptions (Console.Out);
        Environment.Exit (0);
    }

    List<string> options (string [] args)
    {
        var optionSet = new OptionSet () {
            { "d|debug",   "debugging",    v => debugs  = v != null },
            { "f|force",   "update page",  v => force   = v != null },
            { "h|?|help",  "display help", v => help    = v != null },
            { "v|verbose", "vebosity",     v => verbose = v != null },
        };

        List<string> extras = optionSet.Parse (args);

        if (help)
        {
            showHelp (optionSet);
        }

        return extras;
    }

    private bool isBClass (Page talk)
    {
        talk.Load ();
        bool assessed = talk.MilHist.ProjectTemplate.Class.Equals ("B");
        Debug.WriteLine (talk.Article.Title + " assessed B class " + assessed);
        return assessed;
    }
    
	private void report (string month, List<string> titles)
	{
		string summary = "AutoCheck report for " + month;
       	Page coordinatorsPage = new Page (bot, coordinatorsPageTitle);
    	Section section = new Section (coordinatorsPage, summary);
    	section.Add ("The following articles were rated as B class by automatic assessment:\n");
    	titles.Sort ();
    	foreach (var title in titles)
    	{
        	section.Add ("* [[" + title + "]]\n");    		
    	}    	
        section.Add ("~~~~\n");
        section.Save (summary);	
	}
	
    private AutoReport (string [] args)
    {
        bot                 = null;
        int exitStatus      = 0;
        DateTime t          = DateTime.Now;
        string lastDay      = t.PreviousMonthLastDay().Timestamp();
        string firstDay     = t.PreviousMonthFirstDay().Timestamp();
        string month        = t.PreviousMonthLastDay().Month();
        const int batchSize = 1000;
        options (args);

        try
        {
            bot         = new Bot ();
            Debug.Bot   = bot;
            Debug.On    = debugs;
        	Query query = new Query ("MilHistBot", batchSize, lastDay, firstDay);
            var titles  = new List<string>();

            do
            {
                var contributions = bot.Contributions (query).FindAll (x => x.Comment.Equals (comment));
                foreach (Contribution contribution in contributions)
                {
                	var talk = new Page (bot, contribution.Title);
                	if (isBClass (talk))
                	{
	                    Console.WriteLine(talk.Article.Title);
	                    titles.Add (talk.Article.Title);            		
                	}
	            }
            } 
            while (query.Continue);

            if (force && titles.Count > 0)
            {
            	report (month, titles);
            }
        }
        catch (BotException bex)
        {
            Console.WriteLine (bex.Message);
            exitStatus = 1;
        }
        finally
        {
            if (null != bot)
            {
                bot.Close ();
            }
        }
        Environment.Exit (exitStatus);
    }

    static public void Main (string [] args)
    {
        var autoReport = new AutoReport (args);
    }
}
