#!/usr/bin/perl -w

use English;
use strict;
use warnings;

use Data::Dumper;
use DateTime;
use File::Basename;
use File::Find::Rule;
use File::stat;

use FindBin qw($Bin);
use lib qw($Bin $Bin/MilHist);

use Cred;
use MilHist::Bot;

binmode (STDERR, ':utf8');
binmode (STDOUT, ':utf8');

my $cred = new Cred ();
#my $editor = MilHist::Bot->new ($cred) or
#  die "new MediaWiki::Bot failed";

#my $bin = '/data/project/milhistbot/bin';
my $bin = "$ENV{HOME}/tool-milhistbot";
chdir $bin;

my @files = File::Find::Rule->file ()->name ('*.pm', '*.pl')->in ('.');
my %files = map { $ARG => stat ($ARG)->mtime } @files;

foreach my $key (keys %files) {
    my ($name, $dirname, $suffix) = fileparse ($key, '.pl');
    my $jobs = $cred->jobs;
 	my $user;	
    foreach my $job (@$jobs) {
        if ($name eq $job->{name}[0]) {
			$user = $job->{user}[0];
			last;
		}
	}
	next unless $user;   
    my $page = "User:$user/$key";
    my $updated = POSIX::strftime ('%Y-%m-%d %H:%M', gmtime ($files{$key}));
    print $page, "\t\t", $updated, "\n";   
}

exit 0;