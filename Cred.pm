package Cred;
#
# Obtain job credentials from the cred.xml file
#
# History:
#   28 Oct 14 Created
#	 2 Nov 14 Add log file
#   13 Oct 17 Unlink cookie file
#   11 Jan 18 Carp routines not all exported by default
#   19 Feb 18 Add nobots here
#   18 Jun 18 Nobots moved to Bot.pm; Added sendmail_lite; added logs to Credx.xml
#    4 Jul 18 Set permissions on log file
#   29 Nov 18 Allow multiple email notifications

use English;
use strict;
use utf8;
use warnings;

use Carp qw(cluck croak longmess);
use Data::Dumper;
use DateTime;
use File::Basename;
use File::Spec;
use IO::File;
use Net::SMTP;
use XML::Simple;

binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

my %fields;

sub new {
    my $that  = shift;
    my $class = ref ($that) || $that;
    my $self  = {%fields};
    bless $self, $class;

    my ($package, $filename) = caller;
    my ($name, $dirname, $suffix) = fileparse ($filename, '.pl');
    my $xml   = XML::Simple->new ();
    my $credx = File::Spec->catfile ($dirname, '.credx.xml');
    if (! -f $credx) {
    	$credx = File::Spec->catfile ($dirname, 'credx.xml');
    }
    my $config = $xml->XMLin ($credx, ForceArray => 1);
    my $jobs   = $config->{job};
    $self->{jobs} = $jobs;

    my $found = 0;
    foreach my $job (@$jobs) {
        if ($name eq $job->{name}[0]) {
            $self->{name} = $name;
            $self->{user} = $job->{user}[0];
            $found        = 1;
            last;
        }
    }
    Carp::croak "unable to find job '$name' in $credx" unless $found;

    my $creds = $config->{cred};
    foreach my $cred (@$creds) {
        if ($self->{user} eq $cred->{user}[0]) {
            $self->{password} = $cred->{password}[0];
            $self->{mail}     = $cred->{mail};
        }
    }
    $self->{smtphost} = $config->{smtphost};

    # Create the log file
    my $logs = $config->{logs}[0]->{directory}[0];
    $self->{logfile} = File::Spec->catfile ($logs, $name . '.log');
    my $log = IO::File->new ($self->{logfile}, O_CREAT | O_WRONLY | O_APPEND) or
      die "unable to open '$self->{logfile}': $OS_ERROR\n";
    $log->binmode (':utf8');
    $self->{log} = $log;

    return $self;
}

sub timestamp ($) {
    my $self = shift;
    return DateTime->now (time_zone => 'UTC')->strftime ('%H:%M:%S %a %d %b %Y ');
}

sub showtime ($@) {
    my $self = shift;
    my $log  = $self->log ();
    print $self->timestamp (), @ARG;
    print $log $self->timestamp (), @ARG;
}

sub sendmail_net ($$$) {
    my $self = shift;
    my ($mail, $message) = @ARG;

	my $from  = 'milhistbot@tools.wmflabs.org';
	my $to    = $mail->{to}[0];
	my $host  = $self->{smtphost}[0];
    my $subject = 'Error report';
	my $debug = 0;
			
	my $smtp = Net::SMTP->new($host,
		SSL   => 1,
		Debug => $debug,
	) or die "smtp failed $OS_ERROR\n";
		
	$smtp->mail($from);
    $smtp->to($to);

    my $header = <<"HEADER";
To: $to
From: $from
Content-Type: text/html
Subject: $subject

HEADER

    $smtp->data();
    $smtp->datasend($header);
    $smtp->datasend($message);
    $smtp->dataend();

    $smtp->quit();
}

sub sendmail_smtp ($$$) {
    my $self = shift;
    my ($mail, $message) = @ARG;

    require MIME::Lite;

    my $msg = MIME::Lite->new (
        From    => $self->{user},
        To      => $mail->{to}[0],
        Subject => 'Error report',
        Data    => $message,
    );

    $msg->send (
        smtp     => $mail->{smtp}[0],
        AuthUser => $mail->{user}[0],
        AuthPass => $mail->{password}[0],
        Debug    => 0
      ) or
      $self->showtime ("send mail failed\n");
}

sub sendmail_mail ($$$) {
    my $self = shift;
    my ($mail, $message) = @ARG;

    require MIME::Lite;

    my $msg = MIME::Lite->new (
        From    => $self->{user},
        To      => $mail->{to}[0],
        Subject => 'Error report',
        Data    => $message,
    );

    $msg->send () or
      $self->showtime ("send mail failed\n");
}

sub sendmail_exim ($$$) {
    my $self = shift;
    my ($mail, $message) = @ARG;

    open SENDMAIL, "|/usr/sbin/exim -odf -i $mail->{to}[0]" or
      die "unable to open exim\n";

    print SENDMAIL "Subject: Error report\n";
    print SENDMAIL "\n";
    print SENDMAIL $message, "\n";

    close SENDMAIL;
}

sub sendmail ($$) {
    my $self = shift;
    my ($message) = @ARG;

    my $mails = $self->{mail};
    foreach my $mail (@$mails) {
        foreach ($mail->{delivery}[0]) {
            /exim/ and $self->sendmail_exim ($mail, $message);
            /mail/ and $self->sendmail_mail ($mail, $message);
            /net/  and $self->sendmail_net  ($mail, $message);
            /smtp/ and $self->sendmail_smtp ($mail, $message);
        }
    }
}

sub error ($@) {
    my $self    = shift;
    my @message = @ARG;
    my $log     = $self->log ();
    my $message = Carp::longmess ($self->timestamp (), $self->{name}, ': ', @message);

    print $log $self->timestamp (), @ARG;
    $self->sendmail ($message);
    Carp::croak $message;
}

sub warning ($@) {
    my $self    = shift;
    my @message = @ARG;
    my $log     = $self->log ();
    my $message = Carp::longmess ($self->timestamp (), $self->{name}, ': ', @message);

    print $log $self->timestamp (), @ARG;
    $self->sendmail ($message);
    Carp::cluck $message;
}

sub AUTOLOAD {
    my $self = shift;
    my $type = ref ($self) or
      Carp::croak "$self is not an object";
    my $name = our $AUTOLOAD;
    $name =~ s/.*://;
    defined $self->{$name} or
      Carp::croak "Can't access '$name' field in object of class $type";
    $self->{$name} = shift if (@ARG);
    return $self->{$name};
}

sub DESTROY {
    my $self = shift;
    if (defined $self && defined $self->{log}) {
        $self->{log}->close;
        chmod 664, $self->{log};
    }
}

1;
