#!/usr/bin/perl -w

# announcements.pl -- Update the MilHist announcements page
# Usage: announcements.pl
#
#  3 Oct 2014 Created
# 14 Oct 2014 Sort entries in chronological order
# 17 Oct 2014 Handle GA2 etc
# 18 Jun 2018 Use new MilHist::Bot wrapper
# 19 Oct 2018 eval wrapper
# 15 Feb 2019 Unescaped left brace is now deprecated

use English;
use strict;
use utf8;
use warnings;

use Carp;
use Data::Dumper;
use Time::Local;

use Cred;
use MilHist::Bot;
use MilHist::Parser;

binmode (STDERR, ':utf8');
binmode (STDOUT, ':utf8');

my $cred = new Cred ();

my %timestamp;
my %month = (
    'January'   => 0,
    'February'  => 1,
    'March'     => 2,
    'April'     => 3,
    'May'       => 4,
    'June'      => 5,
    'July'      => 6,
    'August'    => 7,
    'September' => 8,
    'October'   => 9,
    'November'  => 10,
    'December'  => 11,
);
my $milhist_template = 'Military history|WikiProject Military history|MILHIST|WPMILHIST';

# log in to the wiki
my $editor = MilHist::Bot->new ($cred) or
  die "new MediaWiki::Bot failed";

sub is_milhist ($) {
    my ($page)   = @ARG;
    my $talkpage = "Talk:$page";
    my $text     = $editor->get_text ($talkpage) or
        return undef;
    my $parser   = new MilHist::Parser ('text' => $text);
    my $template = $parser->find ($milhist_template);
    return $template;
}

sub timestamp ($) {
    my ($candidate) = @ARG;
    my @history = $editor->get_history ($candidate) or
      $editor->error ("Unable get history of '$candidate'");
    my $created = pop @history;
    my ($hour, $min, $sec) = $created->{timestamp_time} =~ /(\d+):(\d+):(\d+)/;
    my ($year, $mon, $day) = $created->{timestamp_date} =~ /(\d+)-(\d+)-(\d+)/;
    return timegm ($sec, $min, $hour, $day, $mon - 1, $year);
}

sub aclass () {
    my $candidates = 'Wikipedia:WikiProject Military history/Assessment/A-Class review';
    my %candidates;
    my $text       = $editor->fetch ($candidates);
    my @candidates = $text =~ /(Wikipedia:WikiProject Military history\/Assessment\/[^}]+)/g;
    foreach my $candidate (@candidates) {
        if ($candidate =~ /Wikipedia:WikiProject Military history\/Assessment\/([^}]+)/) {
            my $page = $1;
            next if $page =~ /^ACR/;
            $cred->showtime ("\t$page\n");
            $candidates{$page} = $candidate;
            $timestamp{$page}  = timestamp ($candidate);
        }
    }
    return %candidates;
}

sub featured ($) {
    my ($candidates) = @ARG;
    my %candidates;
    my $text       = $editor->fetch ($candidates);
    my @candidates = $text =~ /($candidates\/.+\/archive\d+)/g;
    foreach my $candidate (@candidates) {
        if ($candidate =~ /$candidates\/(.+)\/archive\d+/) {
            my $page = $1;
            if (is_milhist ($page)) {
                $cred->showtime ("\t$page\n");
                $candidates{$page} = $candidate;
                $timestamp{$page}  = timestamp ($candidate);
            }
        }
    }
    return %candidates;
}

sub good_article_timestamp ($$) {
    my ($candidate, $template_link) = @ARG;
    my $text = $editor->fetch ($candidate);
    if ($text =~ /$template_link\|(\d+):(\d+), (\d+) (\w+) (\d+)/) {
        my ($hour, $min, $day, $month, $year) = ($1, $2, $3, $4, $5);
        my $sec = 0;
        my $mon = $month{$month};
        return timegm ($sec, $min, $hour, $day, $mon, $year);
    } else {
        $cred->showtime ("Unable to find GA nominee in '$candidate'\n");
        return undef;
    }
}

sub good_article_entry ($$) {
    my ($article, $template_link) = @ARG;
    my $entry = new MilHist::Template ('name' => 'WPMHA/GAN');
    $entry->add (1 => $article)->default (1);
    my $talk     = "Talk:$article";
    my $text     = $editor->fetch ($talk);
    my $parser   = new MilHist::Parser ('text' => $text);
    my $template = $parser->find ($template_link);
    if ($template) {
        my $number = $template->get ('page');
        $entry->add (2 => $number)->default (1);
    }
    return $entry->text;
}

sub good_article_nominees ($$) {
    my ($candidates, $template_link) = @ARG;
    my @milhist;
    my @candidates = $editor->get_pages_in_category ($candidates) or
      $editor->error ("Unable to find '$candidates'");
    foreach my $candidate (@candidates) {
        if ($candidate =~ /Talk:(.+)/) {
            my $page = $1;
            if (is_milhist ($page)) {
                $cred->showtime ("\t$page\n");
                $timestamp{$page} = good_article_timestamp ($candidate, $template_link) or next;
                push @milhist, $page;
            }
        }
    }
    return join '  &bull; ', map { good_article_entry ($ARG, $template_link) } sort { $timestamp{$a} <=> $timestamp{$b} } @milhist;
}

sub good_article_reassessments () {
    my $candidates = 'Wikipedia good article reassessment';
    my %candidates;
    my @candidates = $editor->get_pages_in_category ($candidates) or
      $editor->error ("Unable to find '$candidates'");
    foreach my $candidate (@candidates) {
        if ($candidate =~ /Wikipedia:Good article reassessment\/(.+)\/\d+/) {
            my $page = $1;
            if (is_milhist ($page)) {
                $cred->showtime ("\t$page\n");
                $candidates{$page} = $candidate;
                $timestamp{$page}  = timestamp ($candidate);
            }
        }
    }
    return %candidates;
}

sub peer_reviews () {
    my $candidates = 'Current peer reviews';
    my %candidates;
    my @candidates = $editor->get_pages_in_category ($candidates) or
      $editor->error ("Unable to find '$candidates'");
    foreach my $candidate (@candidates) {
        if ($candidate =~ /Wikipedia:Peer review\/(.+)\/archive\d+/) {
            my $page = $1;
            if (is_milhist ($page)) {
                $cred->showtime ("\t$page\n");
                $candidates{$page} = $candidate;
                $timestamp{$page}  = timestamp ($candidate);
            }
        }
    }
    return %candidates;
}

sub format_up (%) {
    my %pages = (@ARG);
    return join '  &bull; ', map { "[[$pages{$ARG}|$ARG]]" } sort { $timestamp{$a} <=> $timestamp{$b} } keys %pages;
}

$cred->showtime ("started\n");
eval {
    my $announcements = 'Template:WPMILHIST Announcements';
    my $text          = $editor->fetch ($announcements);
    my $bot           = " <!-- Bot generated -->";

    $cred->showtime ("A class reviews:\n");
    my %acr = aclass();
    my $acr = format_up (%acr);
    $text =~ s/A-Class_reviews=.*/A-Class_reviews=$acr $bot/;

    $cred->showtime ("Featured article candidates:\n");
    my %fac = featured  ('Wikipedia:Featured article candidates');
    my $fac = format_up (%fac);
    $text =~ s/featured_article_candidates=.*/featured_article_candidates=$fac/;

    $cred->showtime ("Featured article reviews:\n");
    my %far = featured  ('Wikipedia:Featured article review');
    my $far = format_up (%far);
    $text =~ s/featured_article_reviews=.*/featured_article_reviews=$far $bot/;

    $cred->showtime ("Featured list candidates\n");
    my %flc = featured  ('Wikipedia:Featured list candidates');
    my $flc = format_up (%flc);
    $text =~ s/featured_list_candidates=.*/featured_list_candidates=$flc $bot/;

    $cred->showtime ("Featured list removal candidates\n");
    my %flr = featured  ('Wikipedia:Featured list removal candidates');
    my $flr = format_up (%flr);
    $text =~ s/featured_list_removal_candidates=.*/featured_list_removal_candidates=$flr $bot/;

    $cred->showtime ("Peer review:\n");
    my %pr = peer_reviews ();
    my $pr = format_up (%pr);
    $text =~ s/peer_reviews=.*/peer_reviews=$pr $bot/;

    $cred->showtime ("Good article nominees:\n");
    my $gan = good_article_nominees ('Good article nominees', 'GA nominee');
    $text =~ s/good_article_nominees=.*/good_article_nominees=$gan $bot/;

    $cred->showtime ("Good article reassessments:\n");
    my %gar = good_article_reassessments ();
    my $gar = format_up (%gar);        
    $text =~ s/good_article_reassessments=.*/good_article_reassessments=$gar $bot/;

    $editor->edit (
        {
            page    => $announcements,
            text    => $text,
            summary => "Updating announcements page",
            bot     => 1,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$announcements'");

};
if ($EVAL_ERROR) {
    $cred->error ($EVAL_ERROR);
}
$cred->showtime ("finished\n");
exit 0;
