#!/usr/bin/perl -w
#
# flc.pl -- Pass or fail an Featured Article class review
# 	This Bot runs every day, looking for featured list articles that have been promoted by a delegate
#	If it finds one, it follows the steps involved in promoting or failing it.
# Usage: flc.pl
#
# 29 Nov 2015 Created
# 28 Dec 2015 Create update_featured_list_log
# 11 Jan 2016 Do not leave a blank line on the nomination page
#  Put the FLC star before DEFAULTSORT
# 24 Jan 2016 Featured list removal
# 17 Aug 2016 Corrected typo causing duplicated Featured log entries
#  3 Sep 2017 Correction for new announcements page format
# 16 Nov 2017 Correct revid in article history
# 25 Jun 2018 Use DateTime instead of Date::Calc, which is not available on ToolServer
# 11 Jul 2018 Wrong month format
# 20 Jul 2018 Add missing subroutine
# 10 Sep 2018 New parser routines
# 15 Feb 2019 Unescaped left brace is now deprecated
# 22 Mar 2019 New addStar routine for FA/FL star

use English;
use strict;
use utf8;
use warnings;

use Carp;
use Data::Dumper;
use DateTime;
use File::Basename qw(dirname);
use File::Spec;
use POSIX qw(strftime);

use Cred;
use MilHist::ArticleHistory;
use MilHist::Bot;
use MilHist::Diff;
use MilHist::Notify;
use MilHist::Parser;
use MilHist::Showcase;

binmode (STDERR, ':utf8');
binmode (STDOUT, ':utf8');

# Pages used
my $candidates         = 'Wikipedia:featured list candidates';
my $category           = 'Wikipedia featured list candidates';
my $removal_candidates = 'Wikipedia:featured list removal candidates';
my $removal_category   = 'Wikipedia featured list removal candidates';
my $removal_log        = 'Wikipedia:Featured list removal candidates/log/%M %Y';
my $promoted_log       = 'Wikipedia:Featured list candidates/Featured log/%M %Y';
my $failed_log         = 'Wikipedia:Featured list candidates/Failed log/%M %Y';
my $announcements      = 'Template:Announcements/New featured content';
my $goings_on          = 'Wikipedia:Goings-on';
my $featured_list_log  = 'Template:Featured list log';
my $showcase_a         = 'Wikipedia:WikiProject Military history/Showcase/A';
my $showcase_fl        = 'Wikipedia:WikiProject Military history/Showcase/FL';
my $milhist_template   = 'Military history|WikiProject Military history|MILHIST|WPMILHIST';
my $nomination_page;    # used only for testing

################################################################################
my $sandbox_test = 0;
if ($sandbox_test) {
    $candidates        = 'User:Hawkeye7/sandbox/test1';
    $nomination_page   = 'User:Hawkeye7/sandbox/test2';
    $category          = 'Hawkeye7 test pages';
    $promoted_log      = 'User:Hawkeye7/Featured log/%M %Y';
    $failed_log        = 'User:Hawkeye7/Failed log/%M %Y';
    $announcements     = 'User:Hawkeye7/sandbox/test3';
    $goings_on         = 'User:Hawkeye7/sandbox/test4';
    $featured_list_log = 'User:Hawkeye7/sandbox/test5';
}
################################################################################

my $cred   = new Cred ();
my $editor = MilHist::Bot->new ($cred) or
  die "new MediaWiki::Bot failed";

sub whodunnit ($$) {
    my ($article, $nomination) = @ARG;
    my $old;
    my @history = $editor->get_history ($nomination) or
      $editor->error ("Unable to get history of '$nomination'");

    my $action;
    foreach my $revision (@history) {
        my $text = $editor->get_text ($nomination, $revision->{revid}) or
          $editor->error ("Unable to find '$nomination:$revision->{revid}')");

        my $parser   = new MilHist::Parser ('text' => $text);
        my $template = $parser->find       ('FLCClosed');
        if ($template) {
            $old    = $revision;
            $action = $template->get (1);
        } else {
            $cred->showtime ("\t$article was $action by $old->{user} at $old->{timestamp_date} $old->{timestamp_time}\n");
            my $diff = new MilHist::Diff ('title' => $nomination, 'diff' => $old->{revid}, 'oldid' => $revision->{revid});
            return ($old->{user}, $old->{timestamp_date}, $old->{timestamp_time}, $diff->as_string, $revision->{revid});
        }
    }
}

sub has_been_closed ($$) {
    my ($article, $nomination) = @ARG;
    $cred->showtime ("\tchecking if $nomination has been promoted...\n");
    my $text = $editor->get_text ($nomination) or do {

        # Nomination in progress?
        $editor->warning ("Unable to find nomination page '$nomination')");
        return ();
    };

    my $parser     = new MilHist::Parser ('text' => $text);
    my $flc_closed = $parser->find       ('FLCClosed');
    if ($flc_closed) {

        my $status = $flc_closed->get (1);

        # No timestamp - get it from whodunnit
        my ($coordinator, $date, $time, $diff, $revid) = whodunnit ($article, $nomination);

        # print "date='$date' time='$time'\n";
        $date =~ /(\d+)-(\d+)-(\d+)/;
        my ($year, $month, $day) = ($1, $2, $3);
        $day =~ s/^0//;

        my $dt            = DateTime->new   (year => $year, month => $month, day => $day);
        my $display_month = $dt->month_name ();

        my $timestamp = {
            'DISPLAY_DATE'  => "$time $day $display_month $year",
            'TIME'          => $time,
            'DATE'          => $date,
            'DAY'           => $day,
            'DISPLAY_MONTH' => $display_month,
            'MONTH'         => $month,
            'YEAR'          => $year,
            'USER'          => $coordinator,
            'DIFF'          => $diff,
            'REVID'         => $revid,
            'STATUS'        => $status,
        };

        return ($status, $timestamp);
    }

    return ();
}

sub is_milhist ($) {
    my ($talkpage)   = @ARG;
    my $text     = $editor->fetch ($talkpage);
    my $parser   = new MilHist::Parser ('text' => $text);
    my $template = $parser->find ($milhist_template);
    return $template;
}

sub add_to_fl_showcase ($) {
    my ($article) = @ARG;
    my $showcase_text = $editor->fetch ($showcase_fl);
    my $showcase      = new MilHist::Showcase ($showcase_text);
    $cred->showtime ("\tAdding to the Featured List showcase\n");
    $showcase->add  ($article);
    $editor->edit (
        {
            page    => $showcase_fl,
            text    => $showcase->text,
            summary => "'$article' has been promoted to Featured List  status",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$showcase_fl'");
}

sub remove_from_a_class_showcase ($) {
    my ($article) = @ARG;
    my $showcase_text = $editor->fetch ($showcase_a);
    my $showcase      = new MilHist::Showcase ($showcase_text);
    my $found         = $showcase->del ($article);
    if ($found) {
        $cred->showtime ("\tRemoving from the A class showcase\n");
        $editor->edit (
            {
                page    => $showcase_a,
                text    => $showcase->text,
                summary => "'$article' has been promoted to Featured List status",
                minor   => 0,
            }
          ) or
          $editor->error ("unable to edit '$showcase_a'");
    }
    return $found;
}

sub remove_from_fl_showcase ($) {
    my ($article) = @ARG;
    my $showcase_text = $editor->fetch ($showcase_fl);
    my $showcase      = new MilHist::Showcase ($showcase_text);
    my $found         = $showcase->del ($article);

    if ($found) {
        $editor->edit (
            {
                page    => $showcase_fl,
                text    => $showcase->text,
                summary => "'$article' has been delisted",
                minor   => 0,
            }
          ) or
          $editor->error ("unable to edit '$showcase_fl'");
    }
    return $found;
}

sub nomination ($$) {
    my ($talk, $template_name) = @ARG;
    my $text = $editor->fetch ($talk);
    my $parser   = new MilHist::Parser ('text' => $text);
    my $template = $parser->find       ($template_name);
    my $path     = $template->get      (1);
    my $nomination = "Wikipedia:$template_name/$path";
    return $nomination;
}

sub archive_nomination ($$$$) {
    $cred->showtime ("\tArchiving the nomination\n");
    my ($nomination, $comment, $summary, $timestamp) = @ARG;
    my $text = $editor->fetch ($nomination);
    my $parser = new MilHist::Parser ('text' => $text);

    my $status      = $timestamp->{STATUS};
    my $coordinator = $timestamp->{USER};
    my $diff        = $timestamp->{DIFF};
    my $result      = "'''$status''' by [[User:$coordinator|$coordinator]] via ~~~~ [$diff]";

    my $fl_top_template = new MilHist::Template ('name' => 'subst:Fl top');
    $fl_top_template->add ('result' => $result);
    $parser->addTop ($fl_top_template, "\n");

    my $fl_bottom_template = new MilHist::Template ('name' => 'subst:Fl bottom');
    $parser->addBottom ("\n", $fl_bottom_template, "\n");
    $text = $parser->text ();

    $editor->edit (
        {
            page    => $nomination,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$nomination'");

}

sub remove_nomination_from_candidates_page ($$) {
    $cred->showtime ("\tRemoving the nomination from the candidates page\n");
    my ($nomination, $summary) = @ARG;
    my $text = $editor->fetch ($candidates);
    $text =~ s/\{\{\Q$nomination\E\}\}\s*\n//s;

    $editor->edit (
        {
            page    => $candidates,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$candidates'");
}

sub add_nomination_to_log_page ($$$$) {
    $cred->showtime ("\tAdding the nomination to the log page\n");
    my ($nomination, $summary, $timestamp, $log) = @ARG;

    $log =~ s/%M/$timestamp->{DISPLAY_MONTH}/;
    $log =~ s/%Y/$timestamp->{YEAR}/;

    my $text = $editor->get_text ($log);
    if (!defined $text) {
        $text = "\{\{Featured list log\}\}\n\{\{TOClimit|3\}\}\n\n";
    }
    die "no bots allowed on '$log'"
      unless $editor->allow_bots ($text, $cred->user);

    $text =~ s/(\{\{TOClimit\|\d+\}\}\n\n)/$1\{\{$nomination\}\}\n/s;

    $editor->edit (
        {
            page    => $log,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$log'");
}

sub update_article_page ($$) {
    $cred->showtime ("\tUpdating the article page\n");
    my ($article, $summary) = @ARG;
    my $text = $editor->fetch ($article);
    my $parser = new MilHist::Parser ('text' => $text);
    my $featured_list_template = new MilHist::Template ('name' => 'Featured list');
    $parser->addStar ($featured_list_template);
    $text = $parser->text ();

    $editor->edit (
        {
            page    => $article,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$article'");
}

sub update_announcements_page ($$) {
    $cred->showtime ("\tUpdating the announcements page\n");
    my ($article, $summary) = @ARG;

    my $text = $editor->fetch ($announcements);

    my $in_list_section = 0;
    my $section_max;
    my @input_lines = split /\n/, $text;
    my @output_lines;
    foreach (@input_lines) {
        if (/<!-- Lists \((\d+), most recent first\) -->/) {
            $section_max = $1;
            $in_list_section++;
            my $a = $article;
            if ($a =~ s/List of //) {
            	$a = ucfirst $a;
                push @output_lines, $ARG, "* [[$article|$a]]";
            } else {
                push @output_lines, $ARG, "* [[$article]]";
            }
            next;
        }
        if ($in_list_section) {
            if (/^$|<\/div>|<!-- Pictures \(\d+, most recent first\) -->/) {
                $in_list_section = 0;
            } elsif ($in_list_section < $section_max) {
                $in_list_section++;
            } else {
                next;
            }
        }
        push @output_lines, $ARG;
    }

    $text = join "\n", @output_lines;

    $editor->edit (
        {
            page    => $announcements,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$announcements'");
}

sub text_to_month ($) {
    my ($month) = @ARG;
    my %months = (
        'January'   => 1,
        'February'  => 2,
        'March'     => 3,
        'April'     => 4,
        'May'       => 5,
        'June'      => 6,
        'July'      => 7,
        'August'    => 8,
        'September' => 9,
        'October'   => 10,
        'November'  => 11,
        'December'  => 12
    );
    return $months{$month};
}

sub update_goings_on_page ($$$) {
    $cred->showtime ("\tUpdating the goings_on page\n");
    my ($article, $summary, $timestamp) = @ARG;

    my $text = $editor->fetch ($goings_on);

    my ($m, $d, $y) = $text =~ /week starting Sunday, \[\[(\w+) (\d+)\]\], \[\[(\d+)\]\]/;
    my $goings_on_dt = DateTime->new (year => $y, month => text_to_month ($m), day => $d);
    my $closure_dt   = DateTime->new (
        year  => $timestamp->{YEAR},
        month => $timestamp->{MONTH},
        day   => $timestamp->{DAY}
    );
    if (DateTime->compare ($goings_on_dt, $closure_dt) > 0) {
        $cred->warning (
"WARNING: Article dated $timestamp->{DAY} $timestamp->{DISPLAY_MONTH} $timestamp->{YEAR} but goings on page is for week starting $d $m $y -- skipping\n"
        );
        
        my $adams_page = 'User talk:Adam Cuerden';
        my $adams_text = $editor->fetch ($adams_page);
        $adams_text .= "\n==Message from FACBOT==\n" .
        "Hi! I'm your FACBot. " .
        "I just wanted to inform you that $article was promoted on $timestamp->{DAY} $timestamp->{DISPLAY_MONTH} $timestamp->{YEAR} " .
        "but the goings on page is for week starting $d $m $y. I'm adding it to the current page. ~~~~\n";
        my $adams_summary = "Goings on page issue with $article";
                
	    $editor->edit (
	        {
	            page    => $adams_page,
	            text    => $adams_text,
	            summary => $adams_summary,
	            minor   => 0,
	        }
	      ) or
	      $editor->warning ("unable to edit '$adams_page'");
    }

    my $in_list_section = 0;
    my @input_lines     = split /\n/, $text;
    my @output_lines;
    foreach (@input_lines) {
        if (/Wikipedia:Featured lists/) {
            $in_list_section = 1;
        }
        if ($in_list_section) {
            if (/^$|Wikipedia:Featured pictures/) {
                $in_list_section = 0;
                push @output_lines, "* [[$article]] ($timestamp->{DAY} $timestamp->{DISPLAY_MONTH})";
            }
        }
        push @output_lines, $ARG;
    }

    $text = join "\n", @output_lines;

    $editor->edit (
        {
            page    => $goings_on,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$goings_on'");
}

sub update_featured_list_log ($$$) {
    $cred->showtime ("\tUpdating the featured list log\n");
    my ($status, $summary, $timestamp) = @ARG;

    my $text = $editor->fetch ($featured_list_log);

    my $year  = $timestamp->{YEAR};
    my $month = $timestamp->{DISPLAY_MONTH};

    sub new_month ($$$$$$) {
        my ($month, $year, $promoted, $failed, $kept, $removed) = @ARG;

        my @new_year =
          ($month eq 'January') ? ('|-', "|colspan=\"3\"|'''$year'''") : ();
        return join "\n", @new_year, '|-', "|$month",
          "|[[Wikipedia:Featured list candidates/Featured log/$month $year|$promoted&nbsp;promoted]]",
          "|[[Wikipedia:Featured list candidates/Failed log/$month $year|$failed&nbsp;failed]]",
          "|[[Wikipedia:Featured list removal candidates/log/$month $year|$removed&nbsp;removed/$kept&nbsp;kept]]",
          "|}";
    }

    foreach ($text) {
        if ($status eq 'promoted') {
            if (/(\|\[\[Wikipedia:Featured list candidates\/Featured log\/$month $year\|)(\d+)&nbsp;promoted/) {
                my $match = $1;
                my $count = $2 + 1;
                s/\Q$match\E\d+&nbsp;promoted/$match$count&nbsp;promoted/;
            } else {
                my $new_month = new_month ($month, $year, 1, 0, 0, 0);
                s/\|}/$new_month/;
            }

        } elsif ($status eq 'failed') {
            if (/(\|\[\[Wikipedia:Featured list candidates\/Failed log\/$month $year\|)(\d+)&nbsp;failed/) {
                my $match = $1;
                my $count = $2 + 1;
                s/\Q$match\E\d+&nbsp;failed/$match$count&nbsp;failed/;
            } else {
                my $new_month = new_month ($month, $year, 0, 1, 0, 0);
                s/\|}/$new_month/;
            }

        } elsif ($status eq 'kept') {
            if (/(\|\[\[Wikipedia:Featured list removal candidates\/log\/$month $year\|)(\d+)&nbsp;removed\/(\d+)&nbsp;kept/) {
                my $match   = $1;
                my $removed = $2;
                my $kept    = $3 + 1;
                s/\Q$match\E.+/$match$removed&nbsp;removed\/$kept&nbsp;kept]]/;
            } else {
                my $new_month = new_month ($month, $year, 0, 0, 1, 0);
                s/\|}/$new_month/;
            }

        } elsif ($status eq 'removed') {
            if (/(\|\[\[Wikipedia:Featured list removal candidates\/log\/$month $year\|)(\d+)&nbsp;removed\/(\d+)&nbsp;kept/) {
                my $match   = $1;
                my $removed = $2 + 1;
                my $kept    = $3;
                s/\Q$match\E.+/$match$removed&nbsp;removed\/$kept&nbsp;kept]]/;
            } else {
                my $new_month = new_month ($month, $year, 0, 0, 0, 1);
                s/\|}/$new_month/;
            }

        } else {
            die "unknown status: '$status'";
        }
    }

    $editor->edit (
        {
            page    => $featured_list_log,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$featured_list_log'");
}

sub get_revid ($$$) {
    my ($page, $date, $time) = @ARG;
    my @history = $editor->get_history ($page) or
      $editor->error ("Unable to get history of '$page'");
    foreach my $history (@history) {
        if (
            $history->{timestamp_date} le $date ||
            ($history->{timestamp_date} eq $date &&
                $history->{timestamp_time} le $time)
        ) {
            return $history->{revid};
        }
    }
    $editor->error ("Unable to get revid of '$page')");
}

sub update_article_history ($$$$$$$) {
    my ($article_history, $action, $date, $link, $result, $oldid, $current_status) = @ARG;
    $article_history->add_action ($action, $date, $link, $result, $oldid);
    $article_history->add ('currentstatus' => "$current_status\n")
      if $current_status;
}

sub add_pr_to_history ($$) {
    my ($article_history, $page) = @ARG;
    my $parser   = $article_history->parser ();
    my $template = $parser->find ('^(oldpeerreview|Old Peer Review)$');
    if (defined $template) {
        $cred->showtime ("\tFound old Peer Review\n");
        my $name    = $template->get ('name') // $template->get (1) // $page;
        my $archive = $template->get ('archive');
        my $link = "Wikipedia:Peer_review/$name" . ($archive ? "/archive$archive" : '');
        my ($history) = $editor->get_history ($link) or do {
        	$editor->warning ("Unable to get history of '$link'");
        	return;
        };
        my $date  = $history->{timestamp_date};
        my $revid = $template->get ('ID');
        if (! defined $revid) {
        	my $time  = $history->{timestamp_time};
        	$revid = get_revid ($page, $date, $time);
        }
        $parser->delete ($template);
        $article_history->add_action ('PR', $date, $link, 'reviewed ', $revid);
    }
}

sub update_talk_page ($$$$$$) {
    $cred->showtime ("\tUpdating the talk page\n");
    my ($status, $article, $talk, $nomination, $summary, $timestamp) = @ARG;

    my $text  = $editor->fetch ($talk);
    my $revid = get_revid ($article, $timestamp->{DATE}, $timestamp->{TIME});

    my $parser = new MilHist::Parser ('text' => $text);
    my $article_history = new MilHist::ArticleHistory ('parser' => $parser);

    # add an old Peer review, if any, to the article history
    add_pr_to_history ($article_history, $article);

    if ($status eq 'promoted') {

        # Remove the FLC card
        my $template = $parser->find ('^featured list candidates$');
        $parser->delete ($template);
        update_article_history ($article_history, 'FLC', $timestamp->{DISPLAY_DATE}, $nomination, 'promoted', $revid, 'FL');

        # Update the class for all projects
        $parser->update_parameter ('class' => 'FL');

    } elsif ($status eq 'failed') {

        # Remove the FLC card
        my $template = $parser->find ('^featured list candidates$');
        $parser->delete ($template);
        update_article_history ($article_history, 'FLC', $timestamp->{DISPLAY_DATE}, $nomination, 'failed', $revid, 'FFLC');

    } elsif ($status eq 'kept') {

        # Remove the FLR card
        my $template = $parser->find ('^featured list removal candidates$');
        $parser->delete ($template);
        update_article_history ($article_history, 'FLR', $timestamp->{DISPLAY_DATE}, $nomination, 'kept', $revid, 'FL');

    } elsif ($status eq 'removed') {

        # Remove the FLR card
        my $template = $parser->find ('^featured list removal candidates$');
        $parser->delete ($template);
        update_article_history ($article_history, 'FLR', $timestamp->{DISPLAY_DATE}, $nomination, 'removed', $revid, 'FFL');

        # Update the class for all projects
        $parser->update_parameter ('class' => 'List');

    } else {
        die "unknown status: '$status'";
    }
    $text = $parser->text ();

    $editor->edit (
        {
            page    => $talk,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$talk'");
}

sub remove_nomination_from_removal_candidates_page ($$) {
    $cred->showtime ("\tRemoving the nomination from the removal candidates page\n");
    my ($nomination, $summary) = @ARG;
    my $text = $editor->fetch ($removal_candidates);
    $text =~ s/\{\{\Q$nomination\E\}\}\s*\n//s;

    $editor->edit (
        {
            page    => $removal_candidates,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$candidates'");
}

sub add_nomination_to_removal_log_page ($$$$$) {
    $cred->showtime ("\tAdding the nomination to the log page\n");
    my ($status, $nomination, $summary, $timestamp, $log) = @ARG;

    $log =~ s/%M/$timestamp->{DISPLAY_MONTH}/;
    $log =~ s/%Y/$timestamp->{YEAR}/;

    my $text = $editor->get_text ($log);
    if (!defined $text) {
        $text = "\{\{Featured list log\}\}\n\{\{TOClimit|3\}\}\n==Keep==\n\n==Delist==\n";
    }
    die "no bots allowed on '$log'" unless $editor->allow_bots ($text);

    if ($status eq 'kept') {
        $text =~ s/(==Keep==)/$1\n\{\{$nomination\}\}\n/s;
    } elsif ($status eq 'removed') {
        $text =~ s/(==Delist==)/$1\n\{\{$nomination\}\}\n/s;
    }

    $editor->edit (
        {
            page    => $log,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$log'");
}

sub remove_star_from_article_page ($$) {
    $cred->showtime ("\tUpdating the article page\n");
    my ($article, $summary) = @ARG;
    my $text = $editor->fetch ($article);
    $text =~ s/\{\{featured list\}\}\s*\n//is;

    $editor->edit (
        {
            page    => $article,
            text    => $text,
            summary => $summary,
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$article'");
}

sub notify_nominator ($$$) {
	my ($coordinator, $article, $nomination) = @ARG;
	
	my $notify = new MilHist::Notify (
		'editor'	  => $editor,
	    'page'        => $article,
	    'type'        => 'list',
	    'discussion'  => $nomination,
	    'coordinator' => $coordinator,
	);
	    
	$notify->send;		
}


sub promoted ($$$$) {
    my ($article, $talk, $nomination, $timestamp) = @ARG;
    my $coordinator = $timestamp->{USER};
    my $status      = $timestamp->{STATUS};

    $cred->showtime ("\tpromoting $article\n");
    my $comment = "The list was '''$status''' by $coordinator via";
    my $summary = "[[$article]] promoted to Featured List";

    update_talk_page ('promoted', $article, $talk, $nomination, $summary, $timestamp);
    remove_nomination_from_candidates_page ($nomination, $summary);
    archive_nomination ($nomination, $comment, $summary, $timestamp);
    add_nomination_to_log_page ($nomination, $summary, $timestamp, $promoted_log);
    update_article_page ($article, $summary);
    
    update_announcements_page ($article, $summary);
    update_featured_list_log ('promoted', $summary, $timestamp);
    update_goings_on_page ($article, $summary, $timestamp);

    if (is_milhist ($talk)) {
        remove_from_a_class_showcase ($article);
        add_to_fl_showcase           ($article);
    }
    
    notify_nominator ($coordinator, $article, $nomination);
    $cred->showtime ("\tdone\n");
}

sub failed ($$$$) {
    my ($article, $talk, $nomination, $timestamp) = @ARG;
    my $coordinator = $timestamp->{USER};
    my $status      = $timestamp->{STATUS};

    $cred->showtime ("\tnot promoting $article\n");
    my $comment = "The list was '''$status''' by $coordinator via";
    my $summary = "[[$article]] not promoted to Featured List";

    update_talk_page ('failed', $article, $talk, $nomination, $summary, $timestamp);
    remove_nomination_from_candidates_page ($nomination, $summary);
    archive_nomination ($nomination, $comment, $summary, $timestamp);
    add_nomination_to_log_page ($nomination, $summary, $timestamp, $failed_log);
    update_featured_list_log ('failed', $summary, $timestamp);

    $cred->showtime ("\tdone\n");
}

sub kept ($$$$) {
    my ($article, $talk, $nomination, $timestamp) = @ARG;
    my $coordinator = $timestamp->{USER};
    my $status      = $timestamp->{STATUS};

    $cred->showtime ("\tkeeping $article\n");
    my $comment = "The list was '''$status''' by $coordinator via";
    my $summary = "[[$article]] kept as Featured List";

    update_talk_page ('kept', $article, $talk, $nomination, $summary, $timestamp);
    remove_nomination_from_removal_candidates_page ($nomination, $summary);
    archive_nomination ($nomination, $comment, $summary, $timestamp);
    add_nomination_to_removal_log_page ('kept', $nomination, $summary, $timestamp, $removal_log);
    update_featured_list_log ('kept', $summary, $timestamp);

    $cred->showtime ("\tdone\n");
}

sub removed ($$$$) {
    my ($article, $talk, $nomination, $timestamp) = @ARG;
    my $coordinator = $timestamp->{USER};
    my $status      = $timestamp->{STATUS};

    $cred->showtime ("\tremoving $article\n");
    my $comment = "The list was '''$status''' by $coordinator via";
    my $summary = "[[$article]] removed from Featured List";

    update_talk_page ('removed', $article, $talk, $nomination, $summary, $timestamp);
    remove_nomination_from_removal_candidates_page ($nomination, $summary);
    archive_nomination ($nomination, $comment, $summary, $timestamp);
    add_nomination_to_removal_log_page ('removed', $nomination, $summary, $timestamp, $removal_log);
    remove_star_from_article_page ($article, $summary);
    update_featured_list_log ('removed', $summary, $timestamp);

    if (is_milhist ($talk)) {
        remove_from_fl_showcase ($article);
    }

    $cred->showtime ("\tdone\n");
}

sub is_older_nomination ($$) {
    my ($nomination, $twenty_days_ago) = @ARG;

    $cred->showtime ($nomination, "\n");

    my @history = $editor->get_history ($nomination) or
      $editor->error ("Unable to get history of '$nomination'");

    my $revision = pop @history;
    $cred->showtime ("\t", $revision->{timestamp_date});

    my $is_older_nomination = $revision->{timestamp_date} lt $twenty_days_ago;
    $cred->showtime (
        $is_older_nomination ? " is older than twenty_days_ago\n" :
          " is NOT older than twenty_days_ago\n"
    );

    return $is_older_nomination;
}

sub move_the_daily_marker () {
    my $text = $editor->fetch ($candidates);

    my @input = split /\n/, $text;
    my @output;
    my $nominations = 0;
    my @older_nominations;
    my $older_nominations = 0;

    my $twenty_days_ago = strftime ('%Y-%m-%d', gmtime (time () - 20 * 24 * 60 * 60));
    $cred->showtime ("twenty days ago was $twenty_days_ago\n");

    foreach (@input) {
        if (/<!--|-->/) {
        } elsif (/==Nominations==/) {
            $nominations = 1;
        } elsif (/==Older nominations==/) {
            $older_nominations = 1;
            $nominations       = 0;
        } elsif ($nominations) {
            if (/\{\{(Wikipedia:Featured list candidates.+)\}\}/) {
                my $nomination = $1;
                if (is_older_nomination ($nomination, $twenty_days_ago)) {
                    push @older_nominations, "\{\{$nomination\}\}";
                    next;
                }
            }
        } elsif ($older_nominations) {
            if (@older_nominations) {
                push @output, @older_nominations;
                $older_nominations = 0;
            }
        }
        push @output, $ARG;
    }

    return unless (@older_nominations);

    $text = join "\n", @output;

    $editor->edit (
        {
            page    => $candidates,
            text    => $text,
            summary => 'update daily marker',
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$candidates'");
}

sub featured_list_candidates () {
    my @candidates = $editor->get_pages_in_category ($category);
    foreach my $talk (@candidates) {
        eval {
            my $article = $talk;
            if ($article =~ s/Talk://) {
                $cred->showtime ($article, "\n");
                my $nomination = nomination ($talk, 'Featured list candidates');
                $cred->showtime ("\t", $nomination, "\n");
                if (my ($status, $timestamp) = has_been_closed ($article, $nomination)) {
                    $cred->showtime ("\t$nomination closed on $timestamp->{DISPLAY_DATE}\n");
                    if ($status eq 'promoted' ||
                        $status eq 'successful') {
                        promoted ($article, $talk, $nomination, $timestamp);
                    } elsif ($status eq 'not promoted' ||
                        $status eq 'failed'    ||
                        $status eq 'withdrawn' ||
                        $status eq 'archived'  ||
                        $status eq 'unsuccessful') {
                        failed ($article, $talk, $nomination, $timestamp);
                    } else {
                        $cred->warning ("WARNING: ''$nomination'' has unknown status '$status'\n");
                    }
                } else {
                    $cred->showtime ("\tnomination is still current\n");
                }
            }
        };
        if ($EVAL_ERROR) {
            $cred->warning ($EVAL_ERROR);
        }
    }
}

sub featured_list_removal_candidates () {
    my @candidates = $editor->get_pages_in_category ($removal_category);
    foreach my $talk (@candidates) {
        eval {
            my $article = $talk;
            if ($article =~ s/Talk://) {
                $cred->showtime ($article, "\n");
                my $nomination = nomination ($talk, 'Featured list removal candidates');
                $cred->showtime ("\t", $nomination, "\n");
                if (my ($status, $timestamp) = has_been_closed ($article, $nomination)) {
                    $cred->showtime ("\t$nomination closed on $timestamp->{DISPLAY_DATE}\n");
                    if ($status eq 'kept') {
                        kept ($article, $talk, $nomination, $timestamp);
                    } elsif ($status eq 'removed' || $status eq 'delisted') {
                        removed ($article, $talk, $nomination, $timestamp);
                    } else {
                        $cred->warning ("WARNING: $nomination has unknown status '$status'\n");
                    }
                } else {
                    $cred->showtime ("\tnomination is still current\n");
                }
            }
        };
        if ($EVAL_ERROR) {
            $cred->warning ($EVAL_ERROR);
        }
    }
}

$cred->showtime ("started\n");
move_the_daily_marker() unless ($sandbox_test);
featured_list_candidates();
featured_list_removal_candidates();
$cred->showtime ("finished\n");

exit 0;
