#!/usr/bin/perl -w

# reviews.pl -- Count reviews per user
# Usage: reviews.pl
#    18 Oct 18 Created
#    13 Feb 19 Use MilHist::Section
# 15 Feb 2019 Unescaped left brace is now deprecated

use English;
use strict;
use utf8;
use warnings;

use DateTime;
use Data::Dumper;

use FindBin qw($Bin);
use lib qw($Bin $Bin/MilHist);

use Cred;
use FAC::Coordinators;
use FLC::Coordinators;
use MilHist::Bot;
use MilHist::Parser;
use MilHist::Section;
use MilHist::Table;
use MilHist::Template;

binmode (STDERR, ':utf8');
binmode (STDOUT, ':utf8');

my $cred   = new Cred ();
my $editor = MilHist::Bot->new ($cred) or
  die "new MediaWiki::Bot failed";

my $fac_coordinators = new FAC::Coordinators ('editor' => $editor);
my $flc_coordinators = new FLC::Coordinators ('editor' => $editor);


sub bold ($) {
    return "'''" . $ARG[0] . "'''";
}

sub dates () {
    my $today         = DateTime->now ();
    my $last_of_month = $today->clone ();
    $last_of_month->subtract ('months' => 1) until $last_of_month->month % 3 == 1;
    $last_of_month = $last_of_month->subtract (days => $today->day ());
    my $first_of_month =
      DateTime->new ('day' => 1, 'month' => $last_of_month->month (), 'year' => $last_of_month->year ())->subtract ('months' => 2);
    my $display_start = $first_of_month->strftime ("%B");
    my $display_end   = $last_of_month->strftime  ("%B %Y");
    my $start         = $first_of_month->strftime ("%Y-%m-%d");
    $last_of_month->add (days => 1);
    my $end = $last_of_month->strftime ("%Y-%m-%d");
    return ($start, $end, $display_start, $display_end);
}

sub list_to_hash ($) {
    my ($list) = @ARG;
    my %a = $list =~ /\[\[(.+?)\|(.+?)\]\]/g;
    return %a;
}

sub aclass_nominators ($) {
    my ($assessment) = @ARG;
    my @nominators = ();
     
    eval { 
    	my $nom_text = $editor->fetch_with_redirect ($assessment); 

	    my @nm         = ($nom_text =~ /<small>''Nominator\(s\):(.+)/ig);
	    @nm or
	        @nm        = ($nom_text =~ /<small>''Nominators:(.+)/ig);
	
		if (@nm) {
		    @nominators = ($nm[0] =~ /\[\[User:(.+?)\|/gi);
	
		    @nominators or
		      @nominators = $nm[0] =~ /\{\{u\|(.+?)\}\}/gi;
		
		    @nominators or
		      @nominators = $nm[0] =~ /\{\{user0\|(.+?)\}\}/gi;
		}
	
	   	@nominators or do {
			my @history = $editor->get_history ($$assessment) or
				$cred->error ("Unable get history of '$$assessment'");
		    my $history = pop @history;
		    @nominators = ($history->{user});
		};
	
	    @nominators or
	      $cred->error ("Unable to find nominator of '$$assessment'\n");
	    
	    my $nominators =  join ' and ', @nominators; 
	    $cred->showtime ("nominated by $nominators\n");
	    
	};

    if ($EVAL_ERROR) {
        $cred->warning ($EVAL_ERROR);
    }

    return @nominators;
}

sub nominators ($$) {
    my ($type, $review) = @ARG;

    my @nominators;
    if ($type eq 'ACR' || $type eq 'FAC' || $type eq 'FLC') {
        @nominators = aclass_nominators ($review);
    } elsif ($type eq 'PR') {
        my $nominator = creator ($$review);
        @nominators = $nominator ? ($nominator) : ();
    } else {
        $cred->warning ("unknown review type ($type) -- skipping\n");
    }
    my %nominators = map { $ARG => 1 } @nominators;
    return %nominators;
}

sub creator ($) {
    my ($review) = @ARG;
    my @history = $editor->get_history ($review);

    # Review may not be started yet
    return undef unless @history;
    my $history = pop @history;
    my $creator = $history->{'user'};
    return $creator;
}

sub reformat ($) {
    my ($d) = @ARG;
    $d =~ /(\d+)-(\d+)-(\d+)/;
    my $dt = DateTime->new ('day' => $3, 'month' => $2, 'year' => $1);
    return $dt->strftime ("%d %B %Y");
}

sub ga_reviews ($$) {
    my ($type, $hash) = @ARG;
    my $reviews = {};
    foreach my $review (keys %$hash) {
        my $reviewer = creator ($review);
        next unless $reviewer;
        ++$reviews->{$reviewer};
        $cred->showtime ("$type: '$review' reviewed by '$reviewer' ($reviews->{$reviewer})\n");
    }
    return $reviews;
}

sub recusing ($$@) {
    my ($reviewer, $date, @history) = @ARG;
    foreach my $history (@history) {
        next unless $reviewer eq $history->{'user'};
        return 1 if $date gt $history->{'timestamp_date'};
        return 1 if $history->{'comment'} =~ /recuse|recusal|recusing/;
    }
    return 0;
}

sub flc_coordinator ($@) {
    my ($reviewer, @history) = @ARG;
    return $flc_coordinators->is_coordinator ($reviewer) ? !recusing ($reviewer, $flc_coordinators->date ($reviewer), @history) : 0;
}

sub fac_coordinator ($@) {
    my ($reviewer, @history) = @ARG;
    return $fac_coordinators->is_coordinator ($reviewer) ? !recusing ($reviewer, $fac_coordinators->date ($reviewer), @history) : 0;
}

sub reviews ($$) {
    my ($type, $hash) = @ARG;
    return ga_reviews ($type, $hash) if ($type eq 'GA');

    my $reviews = {};
    foreach my $review (keys %$hash) {
        my %nominators = nominators ($type, \$review);
        my %reviewers;

        # Zero is possible, as the review page may not have been created yet
        my @history = $editor->get_history ($review);
        foreach my $history (@history) {
            my $reviewer = $history->{'user'};
            next if $reviewer =~ /Bot$/i || $nominators{$reviewer} || $reviewer eq '';
            $reviewers{$reviewer} = 1;
        }

        foreach my $reviewer (keys %reviewers) {
            next if $type eq 'FAC' && fac_coordinator ($reviewer, @history);
            next if $type eq 'FLC' && flc_coordinator ($reviewer, @history);
            ++$reviews->{$reviewer};
            $cred->showtime ("$type: '$review' reviewed by '$reviewer' ($reviews->{$reviewer})\n");
        }
    }
    return $reviews;
}

sub tally ($$$) {
    my ($reviews, $type, $tally) = @ARG;
    foreach my $reviewer (keys %$tally) {
        $reviews->{$reviewer} //= {};
        $reviews->{$reviewer}->{$type} = $tally->{$reviewer};
    }
}

sub award ($) {
    my ($tally) = @ARG;
    my $award =
        ($tally >= 15) ? 'WikiChevrons'
      : ($tally >= 8)  ? 'The Content Review Medal of Merit (Military history)'
      : ($tally >= 4)  ? 'The Milhist reviewing award (2 stripes)'
      :                  'The Milhist reviewing award (1 stripe)';
    return $award;
}

sub nomination ($$$$) {
    my ($reviewer, $tally, $display_start, $display_end) = @ARG;
    my $nomination = new MilHist::Template ('name' => "WPMILHIST Award nomination");
    my $citation = sprintf ("participating in $tally review%s between $display_start and $display_end", ($tally > 1 ? 's' : ''));
    $nomination->add ('nominee'  => $reviewer);
    $nomination->add ('citation' => $citation);
    $nomination->add ('award'    => award ($tally));
    $nomination->add ('status'   => 'nominated');
    my $text = $nomination->text ();
    return $text;
}

sub report ($$$) {
    my ($reviews, $display_start, $display_end) = @ARG;
    my $heading = [ 'User', 'FA', 'FL', 'A-Class', 'GA', 'PR', 'Total', 'Nomination' ];
    my @rows;

    my ($fac_total, $flc_total, $acr_total, $ga_total, $pr_total);
    foreach my $reviewer (keys %$reviews) {
        my $acr = $reviews->{$reviewer}->{'ACR'} // 0;
        my $fac = $reviews->{$reviewer}->{'FAC'} // 0;
        my $flc = $reviews->{$reviewer}->{'FLC'} // 0;
        my $ga  = $reviews->{$reviewer}->{'GA'}  // 0;
        my $pr  = $reviews->{$reviewer}->{'PR'}  // 0;

        next unless $acr > 0;

        my $fac_sp = $reviews->{$reviewer}->{'FAC'} // '';
        my $flc_sp = $reviews->{$reviewer}->{'FLC'} // '';
        my $ga_sp  = $reviews->{$reviewer}->{'GA'}  // '';
        my $pr_sp  = $reviews->{$reviewer}->{'PR'}  // '';

        my $total = $fac + $flc + $acr + $ga + $pr;

        $acr_total += $acr;
        $fac_total += $fac;
        $flc_total += $flc;
        $ga_total  += $ga;
        $pr_total  += $pr;

        my $nomination = nomination ($reviewer, $total, $display_start, $display_end);
        my $row = [ bold ($reviewer) => $fac_sp, $flc_sp, $acr, $ga_sp, $pr_sp, $total, $nomination ];
        push @rows, $row;
    }
    my $grand_total = $fac_total + $flc_total + $acr_total + $ga_total + $pr_total;
    my $row         = [
        bold ('Total') => bold ($fac_total),
        bold ($flc_total), bold ($acr_total), bold ($ga_total), bold ($pr_total), bold ($grand_total), ''
    ];
    push @rows, $row;

    my $table = new MilHist::Table ();
    $table->align ([ 'left', 'right', 'right', 'right', 'right', 'right', 'right', 'left', ]);
    my $text = $table->tabulate ($heading, \@rows);

    return $text;
}

sub post_report ($$$) {
    my ($report, $display_start, $display_end) = @ARG;

    my $awards_page  = 'Wikipedia talk:WikiProject Military history/Awards';
    my $text         = $editor->fetch ($awards_page);
    my $parser       = new MilHist::Parser ('text' => $text);
    my $section      = $parser->section ('Quarterly reviewing awards');
    my $section_text = join "\n", $report, '~~~~', "\n";
    my $section_name = "$display_start to $display_end reviewing tallies";
    my $subsection   = new MilHist::Section ('name' => $section_name, 'text' => $section_text, 'level' => 3);
    $section->add ("\n");
    $section->add ($subsection);
    $text = $parser->text;

    $cred->showtime ("Adding report to $awards_page\n");
    $editor->edit (
        {
            page    => $awards_page,
            text    => $text,
            summary => "$display_start to $display_end reviewing tallies",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$awards_page'");
}

sub delete_open ($$) {
    my ($reviews, $is_open) = @ARG;
    foreach my $review (keys %$reviews) {
        if ($is_open->{$review}) {
            delete $reviews->{$review};
        }
    }
}

$cred->showtime ("started\n");

eval {

    my ($start, $end, $display_start, $display_end) = dates();
    my $announcements = 'Template:WPMILHIST Announcements';
    my @history       = $editor->get_history ($announcements) or
      $editor->error ("Unable get history of '$announcements'");

    my (%acr,      %fac,      %flc,      %pr,      %ga);
    my (%acr_open, %fac_open, %flc_open, %pr_open, %ga_open);

    $cred->showtime ("Tallying reviews\n");
    foreach my $history (@history) {
        my $date = $history->{timestamp_date};
        next if $date gt $end;
        last if $date lt $start;

        my $user = $history->{user};
        #print "user=$user\n";
        next unless $user eq 'MilHistBot';

        my $text = $editor->get_text ($announcements, $history->{revid}) or
          $editor->error ("Unable to find '$announcements:$history->{revid}')");
        my $parser   = new MilHist::Parser ('text' => $text);
        my $template = $parser->find ('WPMILHIST Announcements/Shell') or
          die "unable to find WPMILHIST Announcements/Shell in $announcements:$history->{revid}";

        my $fac_list = $template->get ('featured_article_candidates') // '';
        my %fac_list = list_to_hash   ($fac_list);

        my $flc_list = $template->get ('featured_list_candidates') // '';
        my %flc_list = list_to_hash   ($flc_list);

        my $acr_list = $template->get ('A-Class_reviews') // '';
        my %acr_list = list_to_hash   ($acr_list);

        my $pr_list = $template->get ('peer_reviews') // '';
        my %pr_list = list_to_hash   ($pr_list);

        my %ga_list;
        my @ga_nominees = $parser->find ('WPMHA/GAN') or
          die "unable to find good_article_nominees parameter";
        foreach my $nominee (@ga_nominees) {
            my $candidate = join '', 'Talk:', $nominee->get (1), '/GA', ($nominee->get (2) // 1);
            $ga_list{$candidate} = 1;
        }

        if ($date eq $end) {
            %acr_open = %acr_list;
            %fac_open = %fac_list;
            %flc_open = %flc_list;
            %pr_open  = %pr_list;
            %ga_open  = %ga_list;
        } else {
            %acr = (%acr, %acr_list);
            %fac = (%fac, %fac_list);
            %flc = (%flc, %flc_list);
            %pr  = (%pr,  %pr_list);
            %ga  = (%ga,  %ga_list);
        }
    }

    $cred->showtime ("Deleting reviews not closed in the period\n");
    delete_open (\%acr, \%acr_open);
    delete_open (\%fac, \%fac_open);
    delete_open (\%flc, \%flc_open);
    delete_open (\%pr,  \%pr_open);
    delete_open (\%ga,  \%ga_open);

    $cred->showtime ("Tallying reviewers\n");
    my $acr_reviews = reviews ('ACR', \%acr);
    my $fac_reviews = reviews ('FAC', \%fac);
    my $flc_reviews = reviews ('FLC', \%flc);
    my $pr_reviews  = reviews ('PR',  \%pr);
    my $ga_reviews  = reviews ('GA',  \%ga);

    my $reviews = {};
    tally ($reviews, 'ACR' => $acr_reviews);
    tally ($reviews, 'FAC' => $fac_reviews);
    tally ($reviews, 'FLC' => $flc_reviews);
    tally ($reviews, 'PR'  => $pr_reviews);
    tally ($reviews, 'GA'  => $ga_reviews);

    $cred->showtime ("Generating report\n");
    my $report = report ($reviews, $display_start, $display_end);

    $cred->showtime ("Posting report\n");
    post_report ($report, $display_start, $display_end);
};
if ($EVAL_ERROR) {
    $cred->error ($EVAL_ERROR);
}
$cred->showtime ("finished\n");
exit 0;
