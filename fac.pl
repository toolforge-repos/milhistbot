#!/usr/bin/perl -w
#
# fac.pl -- Pass or fail an Featured Article class review
# 	This Bot runs every day, looking for featured class articles that have been promoted by a delegate
#	If it finds one, it follows the steps involved in promoting or failing it.
# Usage: fac.pl
#
# 21 Sep 2014 Created
#  2 Sep 2017 Correction for new announcements page format
#  9 Sep 2017 Fix for GA templates missing oldids
# 15 Nov 2017 Guard against not being able to find nomination page
# 23 Feb 2018 allow bots moved to cred; some code for old articles with GA reviews on the talk page
# 26 Apr 2018 Fix for <noinclude>..</noinclude> tags on archive page
# 20 Jul 2018 Fix date comparison
# 23 Jul 2018 Remove has_nested - new add_dyk_to_history
# 25 Jul 2018 Overhaul some routines to use new classes
#  5 Sep 2018 Sort the entries
# 24 Sep 2018 More changes for new parser modules
#  2 Oct 2018 Add MilHist to FA showcase if they are A-class or not
#  5 Oct 2018 Detect nominations that have not been transcluded
#  9 Oct 2018 Use Showcase.pm
# 14 Jan 2019 Good articles list name changes
# 15 Feb 2019 Unescaped left brace is now deprecated
# 22 Mar 2019 New addStar routine for FA/FL star

use English;
use strict;
use utf8;
use warnings;

use Carp;
use Data::Dumper;
use Date::Parse;
use DateTime;
use DateTime::Format::Strptime;
use File::Spec;
use POSIX qw(strftime);
use XML::Simple;

use FindBin qw($Bin);
use lib qw($Bin $Bin/MilHist);

use Cred;
use MilHist::ArticleHistory;
use MilHist::Bot;
use MilHist::Diff;
use MilHist::Notify;
use MilHist::Parser;
use MilHist::Showcase;
use MilHist::Template;

binmode (STDERR, ':utf8');
binmode (STDOUT, ':utf8');

# Pages used
my $candidates_page  = 'Wikipedia:featured article candidates';
my $announcements    = 'Template:Announcements/New featured content';
my $goings_on        = 'Wikipedia:Goings-on';
my $showcase_a       = 'Wikipedia:WikiProject Military history/Showcase/A';
my $showcase_fa      = 'Wikipedia:WikiProject Military history/Showcase/FA';
my $milhist_template = 'Military history|WikiProject Military history|MILHIST|WPMILHIST';

my $cred   = new Cred ();
my $editor = MilHist::Bot->new ($cred) or
  die "new MediaWiki::Bot failed";

sub has_been_closed ($) {
    my ($nomination) = @ARG;
    # $cred->showtime ("checking if $nomination has been closed...\n");
    my $text = $editor->get_text ($nomination) or do {
        return 'no nomination';
    };
    my $parser     = new MilHist::Parser ('text' => $text);
    my $fac_closed = $parser->find ('FACClosed');
    if ($fac_closed) {
        my $status = lc $fac_closed->get (1);
        $cred->showtime ("$nomination has been $status\n");
        if ($status eq 'promoted') {
            return 'promoted';
        } elsif ($status eq 'archived' || $status eq 'withdrawn') {
            return 'archived';
        } else {
            return 'closed';
        }
    } else {
        my $candidates_text = $editor->fetch ($candidates_page);
        if ($candidates_text =~ /\Q$nomination\E/) {
            return 'open';
        } else {
            return 'unopened';
        }
    }
}

sub has_been_promoted ($$$) {
    my ($nomination, $month, $year) = @ARG;
    $cred->showtime ("\tchecking if $nomination has been promoted...\n");
    my $log_page = "Wikipedia:Featured article candidates/Featured log/$month $year";
    my $log_text = $editor->fetch ($log_page);
    $log_text =~ s/_/ /g;
    if ($log_text =~ /\Q$nomination\E/) {
        $cred->showtime ("\tfound $nomination\n");
        return 1;
    }
    $cred->showtime ("\t$nomination NOT promoted\n");
    return 0;
}

sub has_been_archived ($$$) {
    my ($nomination, $month, $year) = @ARG;
    $cred->showtime ("\tchecking if $nomination has been archived...\n");
    my $log_page = "Wikipedia:Featured article candidates/Archived nominations/$month $year";
    my $log_text = $editor->fetch ($log_page);
    $log_text =~ s/_/ /g;
    if ($log_text =~ /\Q$nomination\E/) {
        $cred->showtime ("\tfound $nomination\n");
        return 1;
    }
    $cred->showtime ("\t$nomination NOT archived\n");
    return 0;
}

sub revision ($$) {
	my ($page, $template_name) = @ARG;
	my $old;
	my @history = $editor->get_history ($page) or
		$editor->error ("Unable to get history of '$page'");
	foreach my $revision (@history) {
#		print Dumper $revision, "\n";
#		my $text = $editor->get_text ($page, {'rvstartid'=> $revision->{revid}} ) or
		my $text = $editor->get_text ($page, $revision->{revid} ) or
			$editor->error ("Unable to find '$page:$revision->{revid}')");
	    my $parser = new MilHist::Parser ('text' => $text);
        my $template = $parser->find ($template_name);
		if ($template) {
			$old = $revision;
		} else {
			return ($old, $revision);
		}
	}
}

sub whodunnit ($$) {
	my ($article, $nomination) = @ARG;
	my ($old, $revision) = revision ($nomination, 'FACClosed');
	$cred->showtime ("\t$article was closed by $old->{user} at $old->{timestamp_date} $old->{timestamp_time}\n");
	my $diff = "https://en.wikipedia.org/w/index.php?title=$nomination\&diff=$old->{revid}\&oldid=$old->{revid}";
	$diff =~ s/ /_/g;
#	print $diff, "\n";
	return ($old->{user}, $old->{timestamp_date}, $old->{timestamp_time}, $diff);
}

sub when_nominated ($$) {
	my ($article, $talk) = @ARG;
	my ($old, $revision) = revision ($talk, 'featured article candidates');
	$cred->showtime ("\t$article was nominated by $old->{user} at $old->{timestamp_date} $old->{timestamp_time}\n");
	return $old;
}

sub days_old ($) {
	my ($timestamp) = @ARG;
	my $ydt = DateTime->now ()->subtract (days => 1);
	# print 'yesterday=', $ydt->strftime ('%H:%M %d %b %Y'), "\n";
	my $dt_parser = DateTime::Format::Strptime->new(pattern => '%Y-%m-%d %H:%M:%S');
	my $ts = join ' ', $timestamp->{timestamp_date}, $timestamp->{timestamp_time};
	my $dt = $dt_parser->parse_datetime($ts);
	# print 'timestamp=', $dt->strftime ('%H:%M %d %b %Y'), "\n";
	return $ydt <=> $dt;
}

sub remove_from_candidates ($$) {
    my ($page, $nomination) = @ARG;
    $cred->showtime ("\tRemoving from the candidates page\n");
    my $candidates_text = $editor->fetch ($candidates_page);
    $candidates_text =~ s/\{\{$nomination\}\}//;

    $editor->edit (
        {
            page    => $candidates_page,
            text    => $candidates_text,
            summary => "Removing $page from FAC candidates page",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$candidates_page'");
}

sub update_nomination_page ($$) {
    my ($nomination, $result) = @ARG;
    $cred->showtime ("\tUpdating the nomination page '$nomination'\n");
    my $text = $editor->fetch ($nomination);

    # Remove transcluded article links and featured article tools
    $text =~ s/<noinclude>.+?<\/noinclude>//s;

    # Tag the top and bottom of the page
    my $parser = new MilHist::Parser ('text' => $text);
    my $fa_top_template = new MilHist::Template ('name' => 'Fa top');
    $fa_top_template->add ('result' => $result);
    $parser->addTop ($fa_top_template, "\n");
    my $fa_bottom_template = new MilHist::Template ('name' => 'Fa bottom');
    $parser->addBottom ($fa_bottom_template, "\n");
    $text = $parser->text;

    return $text;
}

sub promote_update_nomination_page ($$$$$) {
    my ($page, $nomination, $user, $date, $diff) = @ARG;
    my $result = "'''promoted''' by [[User:$user|$user]] via ~~~ $date [$diff]";
    my $text   = update_nomination_page ($nomination, $result);

    $editor->edit (
        {
            page    => $nomination,
            text    => $text,
            summary => "Promoting '$page'",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$nomination'");
}

sub archive_update_nomination_page ($$$$$) {
    my ($page, $nomination, $user, $date, $diff) = @ARG;
    my $result = "'''archived''' by [[User:$user|$user]] via ~~~ $date [$diff]";
    my $text   = update_nomination_page ($nomination, $result);

    $editor->edit (
        {
            page    => $nomination,
            text    => $text,
            summary => "Archiving '$page'",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$nomination'");
}

sub good_article_lists  () {
	$cred->showtime ("\tFinding the good article lists\n");
	my $good_articles = 'Wikipedia:Good articles';
	my @good_articles_all = ("$good_articles/all", "$good_articles/all2");
	my @good_article_lists;
	foreach my $good_articles_all (@good_articles_all)
	{
		my $text = $editor->fetch_with_redirect (\$good_articles_all);
		my $parser = new MilHist::Parser ('text' => $text);
		my @good_article_templates = $parser->find ("$good_articles\/[A-Z]");
		push @good_article_lists, map { $ARG->name; } @good_article_templates;
	}
	#foreach (@good_article_lists) {
	#	print $ARG, "\n";
	#}
	return @good_article_lists;
}

sub update_good_article_list ($) {
    my ($article) = @ARG;

    $cred->showtime ("\tFinding $article in the good article list\n");
    my @good_article_lists = good_article_lists();

    my $foundit = 0;
    foreach my $list (@good_article_lists) {
        next if $list =~ /Summary|header/i;

        my $text = $editor->fetch ($list);

        my @input = split /\n/, $text;
        my @output;

        foreach (@input) {
            if (!$foundit) {
                if (/\[\[([^\]\|]+)/) {
                    my $good_article_name = $1;
                    if ($good_article_name eq $article) {
                        $foundit = 1;
                        next;
                    }
                }
            }
            push @output, $ARG;
        }

        if ($foundit) {
            print $cred->showtime ("\tUpdating $article in the good article list\n");
            $text = join "\n", @output;
            $editor->edit (
                {
                    page    => $list,
                    text    => $text,
                    summary => 'update good article list',
                    minor   => 0,
                }
              ) or
              $editor->error ("unable to edit '$list'");

            last;
        }
    }
}

sub promote_update_article_page ($) {
    my ($page) = @ARG;
    $cred->showtime ("\tUpdating the article page\n");
    my $text = $editor->fetch ($page);
    my $parser = new MilHist::Parser ('text' => $text);

    my $good_article_template = $parser->find ("^good article\$");
    if ($good_article_template) {
        $parser->delete ($good_article_template);
        update_good_article_list ($page);
    }

    my $featured_article_template = new MilHist::Template ('name' => 'Featured article');
    $parser->addStar ($featured_article_template);
    $text = $parser->text ();

    $editor->edit (
        {
            page    => $page,
            text    => $text,
            summary => "Promoting '$page' to Featured Article status",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$page'");
}

sub promote_update_featured_topic ($$) {
	my ($article, $ftname) = @ARG;
	my $ftpage = "Wikipedia:Featured topics/$ftname";

    my $text = $editor->fetch ($ftpage);
    my $parser = new MilHist::Parser ('text' => $text);

    my $updated = 0;
    my @input_lines = split /\n/, $text;
    my @output_lines;
    foreach (@input_lines) {
		if (/\{\{classicon\|GA\}\}[ \'\"]+\[\[$article(\||\])/) {
			s/GA/FA/;
			$updated = 1;
        }
        push @output_lines, $ARG;
    }
    $text = join "\n", @output_lines;

	if ($updated) {
	    $editor->edit (
	        {
	            page    => $ftpage,
	            text    => $text,
	            summary => "Promoting '$article' to Featured Article status",
	            minor   => 0,
	        }
	      ) or
	      $editor->error ("unable to edit '$ftpage'");
	}
}

sub promote_update_featured_topic_category ($$) {
	my ($article, $ftname) = @ARG;
	my $category = "Category:Wikipedia featured topics $ftname featured content";

    my $text = $editor->get_text ($category);
    if (! defined $text) {
    	$text = "[[Category:Wikipedia featured topics $ftname]]\n";

	    $editor->edit (
	        {
	            page    => $category,
	            text    => $text,
	            summary => "Promoting '$article' to Featured Article status",
	            minor   => 0,
	        }
	      ) or
	      $editor->error ("unable to edit '$category'");
	}
}

sub get_revid ($$$) {
    my ($page, $date, $time) = @ARG;
    my @history = $editor->get_history ($page) or
      $editor->error ("Unable to get history of '$page'");
    foreach my $history (@history) {
        if ("$history->{timestamp_date} $history->{timestamp_time}" le "$date $time") {
            return $history->{revid};
        }
    }
    $editor->error ("Unable to get revid of '$page')");
}

sub remove_candidacy ($$) {
    my ($talk, $parser) = @ARG;
    my $template = $parser->find ('^featured article candidates$');
    if (defined $template) {
        my $ret = $parser->delete ($template);
		if ($ret < 0) {
			$editor->error ("Did not find candidacy template on '$talk' - possibly under another template");
		}
    } else {
        $editor->error ("Did not find candidacy on '$talk'");
    }
}

sub promote_update_talk_page ($$$$$) {
    my ($page, $talk, $nomination_page, $date, $time) = @ARG;

    $cred->showtime ("\tUpdating the talk page\n");
    my $text = $editor->fetch ($talk);
    my $parser = new MilHist::Parser ('text' => $text);

    # Remove the candidacy
    remove_candidacy ($talk, $parser);

     my $article_history = new MilHist::ArticleHistory (
        'editor' => $editor,
        'parser' => $parser,
        'page'   => $page,
        'talk'   => $talk);

	# Find the ftname, if there is one
	my $ftname = $article_history->get ('ftname');

    # Update the article history
    my $revid = get_revid ($page, $date, $time);
    $article_history->add_action ('FAC', $date, $nomination_page, 'promoted', $revid);

    $article_history->merge ();
    $article_history->sort ();

    # Update the current status
    $article_history->add ('currentstatus' => "FA\n");

	# Update the class for all projects
	my $banner_shell = $parser->find ('WikiProject banner shell');
	if ($banner_shell) {
	    my @projects = $parser->find_parameter ('class');
	    foreach my $project (@projects) {
			$project->delete ('class');
	    }
		$banner_shell->insert ('class' => 'FA');
	} else {
		$parser->update_parameter ('class' => 'FA');
	}

    $text = $parser->text ();

    $editor->edit (
        {
            page    => $talk,
            text    => $text,
            summary => "Promoting '$page' to Featured Article status",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$talk'");

      return $ftname;
}

sub archive_update_talk_page ($$$$$) {
    my ($page, $talk, $nomination_page, $date, $time) = @ARG;

    $cred->showtime ("\tUpdating the talk page\n");
    my $text = $editor->fetch ($talk);
    my $parser = new MilHist::Parser ('text' => $text);

    # Remove the candidacy
    remove_candidacy ($talk, $parser);

     my $article_history = new MilHist::ArticleHistory (
        'editor' => $editor,
        'parser' => $parser,
        'page'   => $page,
        'talk'   => $talk);

    # Update the article history
    my $revid = get_revid ($page, $date, $time);
    $article_history->add_action ('FAC', $date, $nomination_page, 'failed', $revid);

    $article_history->merge ();
    $article_history->sort ();

    # Update the current status
    my $currentstatus = $article_history->get ('currentstatus') // '';
    if ($currentstatus eq '') {
	    my @templates = $parser->find_parameter ('class');
	    foreach my $template (@templates) {
	        my $class = $template->get ('class');
	        if ($class eq 'GA') {
	            $article_history->add ('currentstatus' => "GA\n");
	            $currentstatus = 'GA';
	            last;
	        }
	    }
	}

    my $newstatus = {
		'FFA'     => 'FFA',
		'FFA/GA'  => 'FFA/GA',
		'FFAC'    => 'FFAC',
		'FFAC/GA' => 'FFAC/GA',
		'GA'	  => 'FFAC/GA',
    };

    my $status = $newstatus->{$currentstatus} // 'FFAC';
    $article_history->add ('currentstatus' => "$status\n");

    $text = $parser->text ();

    $editor->edit (
        {
            page    => $talk,
            text    => $text,
            summary => "Updating '$page' after unsuccessful Featured Article nomination",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$talk'");
}

sub remove_candidacy_from_talk_page ($) {
    my ($talk) = @ARG;

    $cred->showtime ("\tUpdating the $talk page\n");
    my $text = $editor->fetch ($talk);
    my $parser = new MilHist::Parser ('text' => $text);

    # Remove the candidacy
    remove_candidacy ($talk, $parser);
    $text = $parser->text ();

    $editor->edit (
        {
            page    => $talk,
            text    => $text,
            summary => "\tRemoving unfinished candidacy from talk page",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$talk'");
}

sub nomination ($$) {
    my ($talk, $template_name) = @ARG;
    my $text = $editor->fetch ($talk);
    my $parser   = new MilHist::Parser ('text' => $text);
    my $template = $parser->find       ($template_name);
    my $path     = $template->get      (1);
    my $nomination = "Wikipedia:$template_name/$path";
    return $nomination;
}

sub update_announcements_page ($) {
    my ($article) = @ARG;
    $cred->showtime ("\tUpdating the announcements page\n");
    my $text = $editor->fetch ($announcements);
    if ($text =~ /\Q$article\E/) {
        $cred->showtime ("\t\tAlready updated -- skipping\n");
        return;
    }

    my $in_list_section = 0;
    my $section_max;
    my @input_lines = split /\n/, $text;
    my @output_lines;
    foreach (@input_lines) {
        if (/<!-- Articles \((\d+), most recent first\) -->/) {
            $section_max = $1;
            $in_list_section++;
            my $a = $article;
            push @output_lines, $ARG, "* [[$article]]";
            next;
        }
        if ($in_list_section) {
            if (/^$|<\/div>|<!-- Topics \(\d+, most recent first\) -->/) {
                $in_list_section = 0;
            } elsif ($in_list_section < $section_max) {
                $in_list_section++;
            } else {
                next;
            }
        }
        push @output_lines, $ARG;
    }

    $text = join "\n", @output_lines;

    $editor->edit (
        {
            page    => $announcements,
            text    => $text,
            summary => "$article promoted to Featured Article status",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$announcements'");
}

sub text_to_month ($) {
    my ($month) = @ARG;
    my %months = (
        'January'   => 1,
        'February'  => 2,
        'March'     => 3,
        'April'     => 4,
        'May'       => 5,
        'June'      => 6,
        'July'      => 7,
        'August'    => 8,
        'September' => 9,
        'October'   => 10,
        'November'  => 11,
        'December'  => 12
    );
    return $months{$month};
}

sub update_goings_on_page ($$) {
    $cred->showtime ("\tUpdating the goings_on page\n");
    my ($article, $closure_dt) = @ARG;
    my $text = $editor->fetch ($goings_on);
    my ($m, $d, $y) = $text =~ /week starting Sunday, \[\[(\w+) (\d+)\]\], \[\[(\d+)\]\]/;
    my $goings_on_dt = DateTime->new (year => $y, month => text_to_month ($m), day => $d);
    if (DateTime->compare ($goings_on_dt, $closure_dt) > 0) {
        my $display_date = $closure_dt->strftime ('%d %B %Y');
        $cred->showtime ("Article closed $display_date but goings on page is for week starting $d $m $y -- skipping\n");
        return;
    }

    if ($text =~ /\Q$article\E/) {
        $cred->showtime ("\t\tAlready updated -- skipping\n");
        return;
    }

    my $display_date = $closure_dt->strftime ('%d %b');
    $display_date =~ s/^0//;
    my $in_list_section = 0;
    my @input_lines     = split /\n/, $text;
    my @output_lines;
    foreach (@input_lines) {
        if (/Wikipedia:Featured articles/) {
            $in_list_section = 1;
        }
        if ($in_list_section) {
            if (/^$|Wikipedia:Featured lists/) {
                $in_list_section = 0;
                push @output_lines, "* [[$article]] ($display_date)";
            }
        }
        push @output_lines, $ARG;
    }

    $text = join "\n", @output_lines;

    $editor->edit (
        {
            page    => $goings_on,
            text    => $text,
            summary => "$article promoted to Featured Article status",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$goings_on'");
}

sub is_milhist ($) {
    my ($talkpage)   = @ARG;
    my $text     = $editor->fetch ($talkpage);
    my $parser   = new MilHist::Parser ('text' => $text);
    my $template = $parser->find ($milhist_template);
    return $template;
}

sub add_to_fa_showcase ($) {
    my ($article) = @ARG;
    my $showcase_text = $editor->fetch ($showcase_fa);
    my $showcase      = new MilHist::Showcase ($showcase_text);
    $cred->showtime ("\tAdding to the Featured Article showcase\n");
    $showcase->add  ($article);
    $editor->edit (
        {
            page    => $showcase_fa,
            text    => $showcase->text,
            summary => "'$article' has been promoted to Featured Article status",
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$showcase_fa'");
}

sub remove_from_a_class_showcase ($) {
    my ($article) = @ARG;
    my $showcase_text = $editor->fetch ($showcase_a);
    my $showcase      = new MilHist::Showcase ($showcase_text);
    my $found         = $showcase->del ($article);
    if ($found) {
        $cred->showtime ("\tRemoving from the A class showcase\n");
        $editor->edit (
            {
                page    => $showcase_a,
                text    => $showcase->text,
                summary => "'$article' has been promoted to Featured Article status",
                minor   => 0,
            }
          ) or
          $editor->error ("unable to edit '$showcase_a'");
    }
    return $found;
}

sub notify_nominator ($$$) {
	my ($coordinator, $article, $nomination) = @ARG;
	
	my $notify = new MilHist::Notify (
		'editor'	  => $editor,
	    'page'        => $article,
	    'type'        => 'article',
	    'discussion'  => $nomination,
	    'coordinator' => $coordinator,
	);
	    
	$notify->send;		
}

my %nominations;

sub nomination_date ($) {
    my ($nomination) = @ARG;
    $editor->fetch_with_redirect (\$nomination);
    my @history = $editor->get_history ($nomination) or
        $editor->error ("Unable to get history of '$nomination'");
    my $oldest_revision = pop @history;
    $nominations{$nomination} = $oldest_revision->{timestamp_date} . 'Z' . $oldest_revision->{timestamp_time};
    return $oldest_revision->{timestamp_date};
}

sub is_older_nomination ($$) {
    my ($nomination, $twenty_days_ago) = @ARG;
    my $nomination_date     = nomination_date ($nomination);
    my $is_older_nomination = $nomination_date lt $twenty_days_ago;
    return $is_older_nomination;
}

sub by_date {
    return $nominations{$b} cmp $nominations{$a};
}

sub parse_date ($) {
    my ($d) = @ARG;
    $d =~ /(\d+)-(\d+)-(\d+)/;
    my $dt           = DateTime->new ('day' => $3, 'month' => $2, 'year' => $1);
    my $display_date = $dt->strftime ('%d %B %Y');
    $display_date =~ s/^0//;
    return ($dt, $display_date);
}

sub move_the_daily_marker () {

    my $today = DateTime->now ()->strftime ("%Y-%m-%d");
    my $user = $cred->user ();
    my @history = $editor->get_history ($candidates_page, 20) or
		$editor->error ("Unable to get history of '$candidates_page'");
		foreach my $history (@history) {
	    if ($history->{'user'} eq $user && $history->{'comment'} eq 'update daily marker') {
	        if ($history->{'timestamp_date'} eq $today) {
                $cred->showtime ("Daily marker already updated\n");
                return;
            }
            last;
	    }
	}

    my $text = $editor->fetch ($candidates_page);
    my @input = split /\n/, $text;
    my @output;
    my @nominations;
    my $nominations = 0;
    my @older_nominations;
    my $older_nominations = 0;
    my $update_required   = 1;

    $cred->showtime ("Move the daily marker\n");
    my $twenty_days_ago = DateTime->now ()->subtract (days => 20)->strftime ("%Y-%m-%d");

    #	print "twenty days ago was $twenty_days_ago\n";

    foreach (@input) {
        if (/Add new nominations at the TOP of this page/) {
            push @output, "==Nominations==",       map { "\{\{$ARG\}\}" } sort by_date @nominations;
            push @output, "\n";
            push @output, "==Older nominations==", map { "\{\{$ARG\}\}" } sort by_date @older_nominations;
            push @output, "\n";
            $older_nominations = 0;

        } elsif (/<!--|-->/) {

        } elsif (/==\s*Nominations\s*==/) {
            $nominations = 1;
            next;

        } elsif (/==\s*Older nominations\s*==/) {
            $older_nominations = 1;
            $nominations       = 0;
            next;

        } elsif ($nominations) {
            if (/\{\{(Wikipedia:Featured article candidates.+)\}\}/) {
                my $nomination = $1;
                if (is_older_nomination ($nomination, $twenty_days_ago)) {
                    push @older_nominations, $nomination;
                    $update_required = 1;
                } else {
                    push @nominations, $nomination;
                }
            }
            next;

        } elsif ($older_nominations) {
            if (/\{\{(Wikipedia:Featured article candidates.+)\}\}/) {
                my $nomination = $1;
                if (is_older_nomination ($nomination, $twenty_days_ago)) {
                    push @older_nominations, $nomination;
                } else {
                    push @nominations, $nomination;
                    $update_required = 1;
                }
            }
            next;
        }

        push @output, $ARG;
    }

    unless ($update_required) {
        $cred->showtime ("No need to reset daily marker\n");
        return;
    }

    $text = join "\n", @output;
    $editor->edit (
        {
            page    => $candidates_page,
            text    => $text,
            summary => 'update daily marker',
            minor   => 0,
        }
      ) or
      $editor->error ("unable to edit '$candidates_page'");

    $cred->showtime ("Daily marker reset\n");
}

$cred->showtime ("started\n");

move_the_daily_marker ();

my @candidates = $editor->get_pages_in_category ('Wikipedia featured article candidates');

foreach my $talk (@candidates) {
    my ($article) = $talk =~ /^Talk:(.+)/ or
        next;

    eval {
        my $nomination = nomination  ($talk, 'Featured article candidates');
        my $status     = has_been_closed ($nomination);
        if ($status eq 'closed' || $status eq 'promoted' || $status eq 'archived') {
            my ($user, $date, $time, $diff) = whodunnit ($article, $nomination);
            my ($dt, $display_date) = parse_date ($date);
            $cred->showtime ("\t$nomination closed on $display_date\n");
            
            if ($status eq 'closed') {
                if (has_been_promoted ($nomination, $dt->month_name, $dt->year)) {
                    $status = 'promoted';
                } elsif (has_been_archived ($nomination, $dt->month_name, $dt->year)) {
                    $status = 'archived';
                }
            }            
            
            if ($status eq 'promoted') {
                my $ftname = promote_update_talk_page ($article, $talk, $nomination, $date, $time);
                if ($ftname) {
                	promote_update_featured_topic ($article, $ftname);
                	promote_update_featured_topic_category ($article, $ftname);
                }
                promote_update_nomination_page ($article, $nomination, $user, $display_date, $diff);
                promote_update_article_page    ($article);
                notify_nominator ($user, $article, $nomination);
                if (is_milhist ($talk)) {
                    remove_from_a_class_showcase ($article);
                    add_to_fa_showcase           ($article);
                }
                update_announcements_page      ($article);
                update_goings_on_page ($article, $dt);

            } elsif ($status eq 'archived') {
                archive_update_talk_page ($article, $talk, $nomination, $date, $time);
                archive_update_nomination_page ($article, $nomination, $user, $display_date, $diff);
            } else {
                $cred->warning ("WARNING: FAC closed but $nomination has NOT been moved to the archive page\n");
            }
        } elsif ($status eq 'open') {
            $cred->showtime ("\t$nomination is still current\n");
        } elsif ($status eq 'no nomination') {
            $cred->showtime ("\t$nomination: no nomination\n");
            # my $old = when_nominated ($article, $talk);
			# if (days_old ($old) > 0) {
            #	remove_candidacy_from_talk_page ($talk);
            # }
        } elsif ($status eq 'unopened') {
            $cred->warning ("WARNING: $nomination has NOT been transcluded on the nomination page\n");
        }
    };
    if ($EVAL_ERROR) {
        $cred->warning ($EVAL_ERROR);
    }
}

$cred->showtime ("finished\n");
exit 0;
