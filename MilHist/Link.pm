#!/usr/bin/perl -w
#
# MilHist::Link.pm - Wiki link class
# Sections consist of a namespace, a name, and a redirect
#

package MilHist::Link;

use Carp;
use English;
use strict;
use warnings;

sub text ($) {
    my ($self) = @ARG;
    my @text;
    if ($self->{'namespace'}) {
        push @text, $self->namespace;
        push @text, ':' unless $self->namespace eq ':';
    }
    push @text, $self->{'name'};
    my $tokens = $self->{'tokens'};
    foreach my $element (@$tokens) {
        if (ref $element) {
            push @text, $element->text;
        } else {
            push @text, $element;
        }
    }
    my $text = join '', '[[', @text, ']]';
    return $text;
}

sub DESTROY {
    my $self = shift;
}

sub AUTOLOAD {
    my $self = shift;
    my $type = ref ($self) or
      Carp::croak "$self is not an object";
    my $name = our $AUTOLOAD;
    $name =~ s/.*://;
    defined $self->{$name} or
      Carp::croak "Can't access '$name' field in object of class $type";
    $self->{$name} = shift if (@ARG);
    return $self->{$name};
}

sub new {
    my $that  = shift;
    my $class = ref ($that) || $that;
    my $self  = {'tokens' => [], @ARG};
    bless $self, $class;
    Carp::croak "name parameter required by $class" unless defined $self->{'name'};
    return $self;
}

1;
