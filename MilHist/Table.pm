#!/usr/bin/perl -w
#
# MilHist::Table.pm - Wiki table class
# The main 
#
# 19 Oct 2018 Created
# 15 Feb 2019 DESTROY method required with AUTOLOAD
# 12 Jan 2024 undef alignment now means no alignment

package MilHist::Table;

use English;
use strict;
use warnings;

my %options = (
    'footer'    => "|}\n\n",
    'noinclude' => 0,
    'align'     => [],
);

sub header ($@) {
	my ($self, @headings) = @ARG;
	my @header;
	push @header, '<noinclude>' if $self->noinclude ();
    push @header, '{| class="wikitable sortable" style="text-align:center"', "\n";
    push @header, '|-', "\n";
    foreach my $heading (@headings) {
        push @header, '! ', $heading, "\n";
    }
    push @header, '</noinclude>' if $self->noinclude ();
    return @header;
}

sub rows ($@) {
    my ($self, @rows) = @ARG;
    my @output;
    my $align = $self->{'align'} // [];
    foreach my $row (@rows) {
        push @output, '|-', "\n";
        my @row = @$row;
        foreach my $i (0..$#row) {
            my $column = $row->[$i];
            my $colsep = 0 == $i ? '|' : '||';
            my $align  = $align->[$i];
            if ($align) {
            	my $style  = ($align eq 'left' || $align eq 'right') ? 'align' : 'style';
         		push @output, $colsep, "$style=\"$align\"| ", $column, ' ';
         	} else {
         		push @output, $colsep, '', $column, ' ';
         	}
         }
        push @output, "\n";
    }
    return @output;
}

sub tabulate ($$$) {
    my ($self, $headings, $rows) = @ARG;
    return join '', $self->header (@$headings), $self->rows (@$rows), $self->footer ();
}

sub DESTROY {
	  my $self = shift;
}

sub AUTOLOAD {
	my $self = shift;
	my $type = ref ($self) or
		Carp::croak "$self is not an object";
	my $name = our $AUTOLOAD;
	$name =~ s/.*://;
	defined $self->{$name} or
		Carp::croak "Can't access '$name' field in object of class $type";
	$self->{$name} = shift if (@ARG);
	return $self->{$name};
}

sub new {
	my $that = shift;
	my $class = ref ($that) || $that;
	my $self = { %options, @ARG };
	bless $self, $class;
	return $self;
}

1;