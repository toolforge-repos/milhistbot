#
# MilHist::Diff.pm - Wiki diff URL class
#
#    22 Mar 2019 Created

package MilHist::Diff;

use English;
use strict;
use utf8;
use warnings;

use Carp;
use URI::URL qw();

sub AUTOLOAD {
    my $self = shift;
    my $type = ref ($self) or
      Carp::croak "$self is not an object";
    my $name = our $AUTOLOAD;
    $name =~ s/.*://;
    defined $self->{$name} or
      Carp::croak "Can't access '$name' field in object of class $type";
    $self->{$name} = shift if (@ARG);
    return $self->{$name};
}

sub DESTROY {
    my $self = shift;
}

sub as_string ($) {
    my $self = shift;
    my $uri  = $self->url ();
    my $url  = new URI::URL ($uri);
    my %hash = %$self;
    delete $hash{'url'};
    $hash{'title'} =~ s/ /_/g;
    $url->query_form (%hash);
    return $url->as_string;
}

sub new {
    my $that  = shift;
    my $class = ref ($that) || $that;
    my $self  = {'url' => 'https://en.wikipedia.org/w/index.php', @ARG};
    bless $self, $class;
    return $self;
}

1;
