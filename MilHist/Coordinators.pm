#!/usr/bin/perl -w
#
# MilHist::Coordinators.pm 
#

package MilHist::Coordinators;

use Carp;
use English;
use strict;
use warnings;

sub is_coordinator ($$) {
    my ($self, $user) = @ARG;
    return $self->{'coordinators'}->{ucfirst $user} // 0;    
}

sub DESTROY {
    my $self = shift;
}

sub new {
    my $that  = shift;
    my $class = ref ($that) || $that;
    my $self  = { @ARG };    
    bless $self, $class;
    Carp::croak "editor parameter required by $class" unless $self->{'editor'};

    my $editor = $self->{'editor'};
    my $text = $editor->fetch ('Template:@MILHIST');
    my @coordinators = ( $text =~ /User:(.+?)\|/g );
    my %coordinators = map { $ARG => 1 } @coordinators; 
    $self->{'coordinators'} = \%coordinators;
    return $self;
}

1;
