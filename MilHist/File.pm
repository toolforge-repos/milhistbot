#!/usr/bin/perl
#
# MilHist::File.pm - Wiki Fileclass
# Files consist of a namespace, a name, a caption, and options
#

package MilHist::File;

use Carp;
use English;
use strict;
use warnings;

sub is_option ($) {
    my $token = shift;
    return ($token =~ /^(border|frame|thumb|thubnail|\d+px|x\d+px|\d+x\d+px|upright|upright=[\.\d]+|left|right|center|none|baseline|sub|super|top|text-top|middle|bottom|text-bottom|link=.*|alt=.+|page=\d+|class=.+|lang=[a-z]{2})$/);
}

sub options ($) {
    my ($self) = @ARG;
    unless (defined $self->{'options'}) {
        my $tokens = $self->{'tokens'};
        my @options;
        my $option = [];
        foreach my $token (@$tokens) {
           if ($token eq '|') {
                push @options, $option;
                $option = [];
            } elsif ($token eq '') {
                next;
            } else {
                $token =~ s/^\s+//;
                $token =~ s/\s+$//;
                push @$option, $token;
            }
        }
        push @options, $option;
        $self->{'options'} = \@options;
    }
    return $self->{'options'};
}

sub _text ($$) {
    my ($self, $elements) = @ARG;
    my @text;           
    foreach my $element (@$elements) {
        if (ref $element) {
            push @text, $element->text;
        } else {
            push @text, $element;
        }
    }
    my $text = join '', @text;
    return $text;    
}

sub _option ($$$) {
    my ($self, $name, $match) = @ARG;
    unless (defined $self->{$name}) {    
        my $value = '';
        my $options = $self->options;
        foreach my $option (@$options) {    
            my $text = $self->_text ($option);
            if ($text =~ /^($match)$/) {
                $value = $text;
                last;
            }    
        }
        $self->{$name} = $value;
    }
    return $self->{$name};
}

sub format ($) {
    my ($self) = @ARG;
    return $self->_option ('format' => 'border|frameless|frame|thumb|thumbnail');
}

sub resizing ($) {
    my ($self) = @ARG;
    return $self->_option ('resizing' => '\d+px|x\d+px|\d+x\d+px|upright|upright=[\.\d]+');
}

sub horizontal_alignment ($) {
    my ($self) = @ARG;
    return $self->_option ('horizontal_alignment' => 'left|right|center|none');
}

sub vertical_alignment ($) {
    my ($self) = @ARG;
    return $self->_option ('vertical_alignment' => 'baseline|sub|super|top|text-top|middle|bottom|text-bottom');
}

sub link ($) {
    my ($self) = @ARG;
    return $self->_option ('link' => 'link=.*');
}

sub alt ($) {
    my ($self) = @ARG;
    return $self->_option ('alt' => 'alt=.+');
}

sub page  ($) {
    my ($self) = @ARG;
    return $self->_option ('page' => 'page=\d+');
}
sub class ($) {
    my ($self) = @ARG;
    return $self->_option ('class' => 'class=.+');
}

sub lang ($) {
    my ($self) = @ARG;
    return $self->_option ('lang' => 'lang=[a-z]{2}');
}

sub caption ($) {
    my ($self) = @ARG;
    unless (defined $self->{'caption'}) {    
        my $caption = '';
        my $options = $self->options;
        foreach my $option (@$options) {   
            my $text = $self->_text ($option);
            next if $text eq '';   
            if (! is_option ($text)) {
                 $caption = $text;
                last;
            }
        }
        $self->{'caption'} = $caption;
    }
    return $self->{'caption'};
}

sub text ($) {
    my ($self) = @ARG;
    my $tokens = $self->{'tokens'};
    my $text = join '', '[[', $self->{'namespace'}, ':', $self->{'name'},  $self->_text ($tokens), ']]';    
    return $text;
}

sub DESTROY {
    my $self = shift;
}

sub AUTOLOAD {
    my $self = shift;
    my $type = ref ($self) or
      Carp::croak "$self is not an object";
    my $name = our $AUTOLOAD;
    $name =~ s/.*://;
    defined $self->{$name} or
      Carp::croak "Can't access '$name' field in object of class $type";
    $self->{$name} = shift if (@ARG);
    return $self->{$name};
}

sub new {
    my $that  = shift;
    my $class = ref ($that) || $that;
    my $self  = {'tokens' => [], @ARG};    
    bless $self, $class;
    Carp::croak "name parameter required by $class" unless defined $self->{'name'};
    return $self;
}

1;
