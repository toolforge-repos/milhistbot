#!/usr/bin/perl -w
#

package MilHist::Notify;

use base qw(MilHist::Template);

use Carp;
use English;
use strict;
use utf8;
use warnings;

sub nominators ($) {
    my ($self) = @ARG;
    my @nominators = ();

    eval {
    	my $nom_text   = $self->editor->fetch_with_redirect (\$self->discussion);
	    my @nm         = ($nom_text =~ /<small.*?>Nominator.*?:(.+)/ig);
		if (@nm) {
		    @nominators = ($nm[0] =~ /\[\[User:(.+?)\]/gi);

		    @nominators or
		        @nominators = ($nm[0] =~ /\[\[User:(.+?)\|/gi);

		    @nominators or
		      @nominators = $nm[0] =~ /\{\{u\|(.+?)\}\}/gi;

		    @nominators or
		      @nominators = $nm[0] =~ /\{\{user0\|(.+?)\}\}/gi;
		}

	   	@nominators or do {
			my @history = $self->editor->get_history ($self->discussion) or
				die ("Unable get history of '$self->nomination'");
		    my $history = pop @history;
		    @nominators = ($history->{user});
		};

	    @nominators or
	      die ("Unable to find nominator of '$self->nomination'\n");

	    foreach (@nominators) {
			s/(\||\]).*//;
	    }
	    
	    @nominators = grep { ! /\// } @nominators;

	    # my $nominators =  join ' and ', @nominators;
	    # $cred->showtime ("nominated by $nominators\n");

	};

    if ($EVAL_ERROR) {
        $self->editor->cred->warning ($EVAL_ERROR);
    }

    return @nominators;
}

sub send ($) {
	my ($self) = @ARG;
	my @nominators = $self->nominators;
	foreach my $nominator (@nominators) {

	    my $user_talk_page = 'User talk:' . $nominator;
	    my $user_talk_text = $self->editor->get_text ($user_talk_page) or do {
            $self->editor->warning ("unable to find '$user_talk_page'");
	        next;
	    };

	    $user_talk_text .= "\n" . $self->text . "\n";

	    $self->editor->cred->showtime ("\tnotifying nominator '$nominator' on  $user_talk_page\n");

	    $self->editor->edit (
	        {
	            page    => $user_talk_page,
	            text    => $user_talk_text,
	            summary => 'Featured content notification',
	            minor   => 0,
	        }
	      ) or
	      $self->editor->warning ("unable to edit '$user_talk_page'");
	}
}

sub new {
    my ($class, %args) = @ARG;
    
   	foreach my $arg ('editor', 'page', 'type', 'discussion', 'coordinator') {
    	croak "$arg required" unless $args{$arg};
    }
    
    my $self = $class->SUPER::new ('name' => 'subst:FC pass talk message', %args);
        
	$self-> add ('page'       => $self->page);
	$self-> add ('type'       => $self->type);
	$self-> add ('discussion' => $self->discussion);
	$self-> add ('poster'     => '{{user0|' . $self->coordinator . '}} via ~~~~');
    
    return $self;
}

1;
