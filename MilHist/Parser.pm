#!/usr/bin/perl
#
# MilHist::Parser.pm - parse Wiki templates
#  8 Oct 2018 add isRedirect
# 11 Oct 2018 Pipes can appear as redirects too
# 22 Oct 2018 Correct out by one error
# 10 Dec 2018 Allow ampersands in parameter names
# 15 Feb 2019 Unescaped left brace is now deprecated
#  4 Mar 2019 Magic words - see https://www.mediawiki.org/wiki/Help:Magic_words
# 22 Mar 2019 New addStar routine for FA/FL star
# 28 Mar 2019 New MilHist::Link module
#  2 Apr 2019 New MilHist::Link module
#  9 Apr 2019 More changes for magic numbers

package MilHist::Parser;

use English;
use strict;
use utf8;
use warnings;

use Carp;
use Data::Dumper;
use MilHist::File;
use MilHist::Link;
use MilHist::Section;
use MilHist::Template;
use MilHist::Template::Parameter;

my %fields;

sub _parse_parameters ($@) {
    my ($self, @parameter_tokens) = @ARG;

    my @parameters;
    my $parameter_count = 0;
    my $template_name = '';
    my $magic = '';
    my $namespace = '';

    while (1) {
        my @tokens;
        while (@parameter_tokens) {
            my $token = shift @parameter_tokens;
            if (ref $token) {
                push @tokens, $token->text;
            } elsif ($token eq '|' || $token eq ':') {
                $magic = $token;
                last;
            } else {
                push @tokens, $token;
            }
        }
        $template_name = join '', $template_name, @tokens;

        if ($magic eq ':') {
            $template_name .= $magic;
            $magic = '';
            if ($template_name =~ /^(Media|Special|Talk|User|User talk|Project|Project talk|File|File talk|MediaWiki|MediaWiki talk|Template|Template talk|Help|Help talk|Category|Category talk|Wikipedia|Wikipedia Talk):/) {
                 next;
            }
        }
        last;
    }

    while (@parameter_tokens) {
        my $name;
        my @tokens;
        my $default;

        my $token = shift @parameter_tokens;

        my $legal_characters = '\s\w\. \%\!\$\#\&\(\)\*\-\\\/\@\?\~\:';
        if (!ref $token && $token =~ /^([$legal_characters]+?)(\s*=\s*)(.*)/s) {
            $name = $1;
            push @tokens, $2, $3;
            $default = 0;

            if ($name =~ /^\d+$/) {
                $parameter_count = $name;
            }
        } else {
            $name = ++$parameter_count;
            push @tokens, $token;
            $default = 1;
        }

        while (@parameter_tokens) {
            $token = shift @parameter_tokens;
            if (ref $token) {
                push @tokens, $token;
            } elsif ($token eq '|') {
                last;
            } else {
                push @tokens, $token;
            }
        }

        my $parameter = new MilHist::Template::Parameter (
            'name'    => $name,
            'tokens'  => \@tokens,
            'default' => $default
        );
        push @parameters, $parameter;
    }
    return ($template_name, \@parameters);
}

sub _parse_template ($$) {
    my ($self, $tokens) = @ARG;

    my $token;
    my @template_tokens;
    while (@$tokens) {
        $token = shift @$tokens;
        if ($token eq '[[') {
            push @template_tokens, $self->_parse_link ($tokens);
        } elsif ($token eq '{{') {
            push @template_tokens, $self->_parse_template ($tokens);
        } elsif ($token eq '}}') {
            last;
        } elsif ($token eq '<!--') {
            push @template_tokens, $self->_parse_comment ($tokens);
        } else {
            push @template_tokens, $token;
        }
    }

    if ($token ne '}}') {
        # Broken template
        # warn 'unclosed template :', @template_tokens,  "\n";
        unshift @$tokens, @template_tokens;
        return '{{';
    }
    my ($name, $parameters) = $self->_parse_parameters (@template_tokens);
    my $template = new MilHist::Template (
        'name'       => $name,
        'parameters' => $parameters,
    );
    return $template;
}

sub _parse_link ($$) {
    my ($self, $tokens) = @ARG;

    my $token;
    my @link_tokens;

    while (@$tokens) {
        $token = shift @$tokens;
        if ($token eq '[[') {
            push @link_tokens, $self->_parse_link ($tokens);
        } elsif ($token eq '{{') {
            push @link_tokens, $self->_parse_template ($tokens);
        } elsif ($token eq '') {
            next;
        } elsif ($token eq ']]') {
            last;
        } elsif ($token eq '<!--') {
            push @link_tokens, $self->_parse_comment ($tokens);
        } else {
            push @link_tokens, $token;
        }
    }

    if ($token ne ']]') {
        # Broken link
        # warn 'unclosed link :', @link_tokens,  "\n";
        unshift @$tokens, @link_tokens;
        return '[[';
    }
    return '[[]]' unless @link_tokens;

    my $ignore;
    my $namespace = '';
    my $colon = '';

    if (defined $link_tokens[0] && $link_tokens[0] eq ':') {
        $colon = shift @link_tokens;
    }

    if (defined $link_tokens[1] && $link_tokens[1] eq ':') {
        ($namespace, $ignore) = splice @link_tokens, 0, 2;
    }

    $namespace = $colon . $namespace if $colon;

    my $name = shift @link_tokens;
    # warn "null name found in link\n" unless defined $name;

    $name = $name->text if ref $name;
    unless ($name) {
    	if ($namespace) {
    		$name = $namespace . ':';
    		$namespace = '';
    	}
   	}

    if ($namespace =~ /^:{0,1}(File|Image)/) {
        my $file = new MilHist::File (
            'namespace' => $namespace,
            'name'      => $name,
            'tokens'   => \@link_tokens,
        );
        return $file;
    } else {
        my $link = new MilHist::Link (
            'namespace' => $namespace,
            'name'      => $name,
            'tokens'   => \@link_tokens,
        );
        return $link;
    }
}

sub _parse_comment ($$) {
    my ($self, $tokens) = @ARG;
    my @comment_tokens = ('<!--');
    while (@$tokens) {
        my $token = shift @$tokens;
        if ($token eq '-->') {
            push @comment_tokens, $token;
            last;
        } else {
            push @comment_tokens, $token;
        }
    }

    return @comment_tokens;
}

sub parse ($$) {
    my ($self, $tokens) = @ARG;
    my @elements;
    while (@$tokens) {
        my $token = shift @$tokens;
        if ($token eq '[[') {
            push @elements, $self->_parse_link ($tokens);
        } elsif ($token eq '{{') {
            push @elements, $self->_parse_template ($tokens);
        } elsif ($token eq '<!--') {
            push @elements, $self->_parse_comment ($tokens);
        } else {
            push @elements, $token;
        }
    }
    return \@elements;
}

sub find_text ($$) {
    my ($self, $match) = @ARG;
    my @matches = ();
    my $elements  = $self->{'elements'};
    foreach my $element (@$elements) {
        if (!ref $element && $element =~ /\Q$match\E/) {
            push @matches, $element;
        }
    }
    return @matches if wantarray;
    return shift @matches;
}

sub find_link_ns ($$) {
    my ($self, $match) = @ARG;
    my @links;
    my $elements = $self->{'elements'};
    foreach my $element (@$elements) {
        if (ref $element eq 'MilHist::Link') {
            my $link = $element;
            if ($link->namespace =~ /$match/i) {
                push @links, $link;
            }
        }
    }
    return @links if wantarray;
    return shift @links;
}

sub find_link ($$) {
    my ($self, $match) = @ARG;
    my @links;
    my $elements = $self->{'elements'};
    foreach my $element (@$elements) {
        if (ref $element eq 'MilHist::Link') {
            my $link = $element;
            if ($link->name =~ /$match/i) {
                push @links, $link;
            }
        }
    }
    return @links if wantarray;
    return shift @links;
}

sub find_file ($$) {
    my ($self, $match) = @ARG;
    my @files;
    my $elements = $self->{'elements'};
    foreach my $element (@$elements) {
        if (ref $element eq 'MilHist::File') {
            my $file = $element;
            if ($file->name =~ /$match/i) {
                push @files, $file;
            }
        }
    }
    return @files if wantarray;
    return shift @files;
}

sub find_section ($$) {
    my ($self, $match) = @ARG;
    my @sections;
    my $elements = $self->{'elements'};
    foreach my $element (@$elements) {
        if (ref $element eq 'MilHist::Section') {
            my $section = $element;
            if ($section->name =~ /$match/i) {
                push @sections, $section;
            }
        }
    }
    return @sections if wantarray;
    return shift @sections;
}

sub _find_parameter ($$$$) {
    my ($self, $templates, $match, $template) = @ARG;
    my $parameters = $template->parameters ();
    foreach my $parameter (@$parameters) {
        if ($parameter->name () =~ /$match/i) {
            push @$templates, $template;
        }
        my $tokens = $parameter->tokens ();
        foreach my $token (@$tokens) {
            if (ref $token eq 'MilHist::Template') {
                $self->_find_parameter ($templates, $match, $token);
            }
        }
    }
}

sub find_parameter ($$) {
    my ($self, $match) = @ARG;
    my @templates = ();
    my $elements  = $self->{'elements'};
    foreach my $element (@$elements) {
        if (ref $element eq 'MilHist::Template') {
            $self->_find_parameter (\@templates, $match, $element);
        }
    }
    return @templates;
}

sub update_parameter ($$$$) {
    my ($self, $match, $value) = @ARG;
    my @templates = $self->find_parameter ($match);
    foreach my $template (@templates) {
        $template->add ($match => $value);
    }
}

sub _find ($$$$) {
    my ($self, $templates, $match, $template) = @ARG;
    my $name = $template->{'name'};
    $name =~ s/\s+$//;
    if ($name =~ /$match/i) {
        push @$templates, $template;
    }
    my $parameters = $template->parameters ();
    foreach my $parameter (@$parameters) {
        my $tokens = $parameter->tokens ();
        foreach my $token (@$tokens) {
            if (ref $token eq 'MilHist::Template') {
                $self->_find ($templates, $match, $token);
            }
        }
    }
}

sub find ($$) {
    my ($self, $match) = @ARG;
    my @templates = ();
    my $elements  = $self->{'elements'};
    foreach my $element (@$elements) {
        if (ref $element eq 'MilHist::Template') {
            $self->_find (\@templates, $match, $element);
        }
    }
    return @templates if wantarray;
    return shift @templates;
}

sub _pos ($$) {
    my ($self, $element) = @ARG;
    my $elements = $self->{'elements'};
    for (my $pos = 0; $pos < scalar @$elements; ++$pos) {
        if (ref $element eq ref $elements->[$pos]) {
            if (ref $element) {
                return $pos if ($element == $elements->[$pos]);
            } else {
                return $pos if ($element eq $elements->[$pos]);
            }
        }
    }
    return -1;
}

sub addTop ($@) {
    my ($self, @elements) = @ARG;
    my $elements = $self->{'elements'};
    unshift @{$self->{'elements'}}, @elements;
}

sub addBottom ($@) {
    my ($self, @elements) = @ARG;
    my $elements = $self->{'elements'};
    foreach my $element (@elements) {
        push @{$self->{'elements'}}, "\n", "\n" if ref $element eq 'MilHist::Section';
        push @{$self->{'elements'}}, $element;
    }
}

sub addAfter ($$@) {
    my ($self, $regex, @elements) = @ARG;
	my @templates = $self->find ($regex);
	my $template = pop @templates;
	if ($template) {
		# print "Found template:", Dumper $template, "\n";
		unshift @elements, "\n";
	}
    my $position = $template ? $self->_pos ($template) + 1 : 0;
    splice @{$self->{'elements'}}, $position, 0, @elements;
}

sub addStar ($@) {
    my ($self, @elements) = @ARG;
    my $hatnotes = '^(Hatnote|Hatnote group|Hatnote inline|About|About other people|About West Toodyay|About-distinguish|About-distinguish2|Broader|Distinguish|Elect|Entomology glossary hatnote|For|For LMST|For2|Highway detail hatnote|Main list|Now-first|Now-num|NPBseason|Offer help|Other hurricanes|Other MeSH codes|Other Pennsylvania townships|Other people|Other places|Other ships|Other stars by Bayer designation|Other uses|Other uses of|Pope Stephen ToP Dab|Qnote|Redirect|Redirect-distinguish|Redirect-distinguish2|Redirect-distinguish6|Redirect-multi|Redirect-several|Redirect-synonym|Redirect2||See introduction|See Wiktionary|Short description|Similar names|Transcluded section|Transcluding article|Transliterationof|WikiIPA|Year dab)$';
	$self->addAfter ($hatnotes, @elements);
}

sub add ($$;$) {
    my ($self, $reference, $element) = @ARG;
    if (ref $reference eq 'MilHist::Section') {
        my $section = $reference;
        $self->addBottom ("\n", "\n", $section->elements);
    } else {
        my $template = $reference;
        my $elements = $self->{'elements'};
        my $pos      = $element ? $self->_pos ($element) : 0;
        splice @{$self->{'elements'}}, $pos, 0, $template;
    }
}

sub replace ($$@) {
    my ($self, $element, @with) = @ARG;
    my $elements = $self->{'elements'};
    my $pos      = $self->_pos ($element);
    croak "element not found" if $pos < 0;
    splice @{$self->{'elements'}}, $pos, 1, @with;
}

sub section ($$) {
    my ($self, $name) = @ARG;
    my $section = new MilHist::Section ('name' => $name, 'parser' => $self);
    return $section;
}

sub isRedirect ($) {
    my ($self) = @ARG;
    return $self->{'redirect'} ? 1 : 0;
}

sub redirect ($) {
    my ($self) = @ARG;
    return $self->{'redirect'};
}

sub delete ($$) {
    my ($self, $template) = @ARG;
    my $pos = $self->_pos ($template);
    if ($pos >= 0) {
        my $elements = $self->{'elements'};
        splice @$elements, $pos, 1;
        if (defined $elements->[$pos] && ref $elements->[$pos] eq '') {
            $elements->[$pos] =~ s/^\s+//;
        }
    }
    return $pos;
}

sub text ($) {
    my ($self) = @ARG;
    my @text;
    my $elements = $self->{'elements'};
    foreach my $element (@$elements) {
        if (ref $element) {
            push @text, $element->text;
        } else {
            push @text, $element;
        }
    }
    my $text = join '', @text;
    return $text;
}

sub elements ($) {
    my ($self) = @ARG;
    return $self->{'elements'};
}

sub new {
    my ($that, @args) = @ARG;
    my $class = ref ($that) || $that;
    my $self  = {%fields, @args};
    bless $self, $class;

    if ($self->{'text'}) {
        my $text = $self->{'text'};
        if ($text =~ /\#REDIRECT \[\[(.+?)\]\]/i) {
            $self->{'redirect'} = $1;
        }
        my @tokens = split /(\[\[|\||\]\]|:|==+|\{\{|\}\}|<!--|-->)/, $text;
        $self->{'elements'} = $self->parse (\@tokens);
    }
    return $self;
}

1;
