#!/usr/bin/perl -w
#
# MilHist::Bot.pm - wrapper for MediaWiki::Bot
# 18 Jun 2018 Created
#  5 Jan 2019 Automatic redirect handling
# 15 Feb 2019 Unescaped left brace is now deprecated

package MilHist::Bot;

use base qw(MediaWiki::Bot);

use English;
use strict;
use utf8;
use warnings;

use Data::Dumper;
use HTTP::Request;
use JSON;
use LWP::UserAgent;
use URI::URL;

sub error ($@) {
    my ($self, @message) = @ARG;
    if ($self->{error}->{code}) {
        push @message, ' (', $self->{error}->{code}, ') : ', $self->{error}->{details};
    }
    $self->{cred}->error (@message);
}

sub warning ($@) {
    my ($self, @message) = @ARG;
    if ($self->{error}->{code}) {
        push @message, ' (', $self->{error}->{code}, ') : ', $self->{error}->{details};
    }
    $self->{cred}->warning (@message);
}

sub allow_bots ($$) {
    my ($self, $text) = @ARG;
    my $user = $self->{cred}->{user};

    return 0 if $text =~ /\{\{[nN]obots\}\}/;
    return 1 if $text =~ /\{\[bB]ots\}\}/;
    if ($text =~ /\{\{[bB]ots\s*\|\s*allow\s*=\s*(.*?)\s*\}\}/s) {
        return 1 if $1 eq 'all';
        return 0 if $1 eq 'none';
        my @bots = split (/\s*,\s*/, $1);
        return (grep $ARG eq $user, @bots) ? 1 : 0;
    }
    if ($text =~ /\{\{[bB]ots\s*\|\s*deny\s*=\s*(.*?)\s*\}\}/s) {
        return 0 if $1 eq 'all';
        return 1 if $1 eq 'none';
        my @bots = split (/\s*,\s*/, $1);
        return (grep $ARG eq $user, @bots) ? 0 : 1;
    }
    return 1;
}

sub edit ($$) {
    my ($self, $options) = @ARG;
    my $page = $options->{'page'};
    my $text = $options->{'text'};
    if ($self->allow_bots ($text)) {
		return $self->SUPER::edit ($options);
    } else {
		$self->{cred}->warning ("no bots allowed on '$page'");
		return undef;
    }
}

sub fetch ($$) {
    my ($self, $page) = @ARG;
    my $text = $self->get_text ($page);
    defined $text or
      $self->error ("Unable to find '$page')");
    return $text;
}

sub fetch_with_redirect ($$) {
    my ($self, $page) = @ARG;
    while (1) {
        my $text = $self->fetch ($$page);
        if ($text =~ /#REDIRECT \[\[(.+)\]\]/i) {
            $$page = $1;
            redo;
        }
        return $text;
    }
}

sub fetch_history ($$$) {
    my ($self, $page, $rvcontinue) = @ARG;

    my $ua = LWP::UserAgent->new ();
    my $url = 'https://en.wikipedia.org/w/api.php';
    my $uri = URI->new($url);

    my $params = [ 
        'action'  => 'query',
        'prop'    => 'revisions',
        'titles'  => $page,
        'rvprop'  => 'ids|timestamp|user|comment',
        'rvslots' => 'main',
        'rvlimit' => 500,
        'formatversion' => '2',
        'format'  => 'json'
    ];
    if ($rvcontinue) {
        push @$params, 'rvstart' => substr ($rvcontinue, 0, 14);
    }

    $uri->query_form(@$params);
    # print $uri->as_string, "\n";

    my $request = HTTP::Request->new (GET => $uri);

    my $response = $ua->request($request);
    $response->is_success or
        die $response->code, ': ', $response->decoded_content;
    #print $response->decoded_content, "\n";

    my $json = from_json ($response->decoded_content);

    if ($json->{'continue'}) {
        $rvcontinue = $json->{'continue'}->{'rvcontinue'};
        # print "continue='$rvcontinue'\n";
    } else {
        $rvcontinue = undef;
        # print "no more\n";
    }

    my $revisions = $json->{'query'}->{'pages'}->[0]->{'revisions'};
    #foreach my $revision (@$revisions) {
    #    print 'revid     => ', $revision->{'revid'}, "\n";
    #    print 'timestamp => ', $revision->{'timestamp'}, "\n";
    #    print 'user      => ', $revision->{'user'}, "\n";
    #}
    return ($revisions, $rvcontinue);
}

#sub get_text ($$;$) {
#	my ($self, $page, $revision) = @ARG;
#	# print 'MediaWiki::Bot version=', $MediaWiki::Bot::VERSION, "\n";
#	my $old_version = $MediaWiki::Bot::VERSION le '5.006003';
	# print $old_version ? 'old version' : 'new version', "\n";
#	if (ref $revision eq 'HASH') {
		# print "HASH value\n";
#		if ($old_version) {
#			return $self->SUPER::get_text ($page, $revision->{'rvstartid'});
#		} else {
#			return $self->SUPER::get_text ($page, $revision);
#		}
#	} elsif (ref $revision eq 'SCALAR') {
		# print "SCALAR value\n";
#		if ($old_version) {
#			return $self->SUPER::get_text ($page, $revision);
#		} else {
#			return $self->SUPER::get_text ($page, {'rvstartid' => $revision});
#		}
#	} else {
		# print "Undef value\n";
#		return $self->SUPER::get_text ($page);
#	}
#}

sub cred () {
    my ($self) = @ARG;
    return $self->{'cred'};
}

sub toss_cookies () {
    my ($self) = @ARG;
    my $cookies = join '-', '.mediawiki', 'bot', $self->{user}, 'cookies';
    unlink $cookies;
}

sub DESTROY {
    my ($self) = @ARG;
    $self->toss_cookies ();
    $self->SUPER::DESTROY if $self->can ('SUPER::DESTROY');
}

sub new {
    my ($class, $cred) = @ARG;

    my $self = $class->SUPER::new (
        {
            assert   => 'bot',
            host     => 'en.wikipedia.org',
            protocol => 'https',
            operator => 'Hawkeye7',
        }
      ) or
      die "new MediaWiki::Bot failed";

    $self->{cred} = $cred;
    $self->{user} = $self->{cred}->{user};
    $self->login (
        {
            username => $self->{cred}->{user},
            password => $self->{cred}->{password},
        }
      ) or
      $self->error ('Login failed');

    $self->toss_cookies ();
    return $self;
}

1;
