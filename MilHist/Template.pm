#!/usr/bin/perl -w
#
# MilHist::Template.pm - Wiki template class
# Templates consist of a name and a list of parameters
#
# 30 June 2018 Created
# 20 July 2018 Newline option
# 15 Feb 2019 DESTROY method required with AUTOLOAD
#  4 Mar 2019 Magic words - see https://www.mediawiki.org/wiki/Help:Magic_words

package MilHist::Template;

use Carp;
use Data::Dumper;
use English;
use strict;
use warnings;

use MilHist::Template::Parameter;

my %fields = ('parameters' => []);

sub find ($$) {
    my $self = shift;
    my $name= shift;
    my @parameters = ();
    my $parameters = $self->{'parameters'};
    foreach my $parameter (@$parameters) {
        if ($parameter->name () =~ /$name/i) {
            push @parameters, $parameter;
        }
    }
    return wantarray ? @parameters : @parameters ? $parameters[0] : undef;
}

sub get ($$) {
    my $self = shift;
    my $name= shift;
    my $value = undef;
    my $parameter = $self->find ($name);
    if (defined $parameter) {
        $value = $parameter->value ();
        $value =~ s/\&\#([0-9a-f]+);/chr($1)/ige;
    }
    return $value;
}

sub delete ($$) {
	my $self = shift;
    my $name= shift;
    my $parameters = $self->{'parameters'};
    for (my $i = 0; $i < scalar @$parameters; $i++) {
        my $parameter = $parameters->[$i];
        if ($parameter->name () =~ /$name/i) {
            splice @$parameters, $i, 1;
            return $parameter;
        }
    }
    return undef;
}

sub add ($$@) {
    my $self = shift;
    my ($name, @tokens) = @ARG;

    my $parameter = $self->find ("^\\s*$name\$");
    if (defined $parameter) {
        my $parameter_tokens = $parameter->tokens;
        unless ($parameter->default) {
            my $equals = shift  @$parameter_tokens;
            unshift @tokens, $equals;
        }
        $parameter->tokens (\@tokens);
    } else {
        unshift @tokens, ' = ';
        $parameter = new MilHist::Template::Parameter (
            'name'    => $name,
            'tokens'  => \@tokens,
            'default' => 0);
        my $parameters = $self->{'parameters'};
        push @$parameters, $parameter;
    }
    return $parameter;
}

sub insert ($$@) {
    my $self = shift;
    my ($name, @tokens) = @ARG;

    my $parameter = $self->find ("^\\s*$name\$");
    if (defined $parameter) {
        my $parameter_tokens = $parameter->tokens;
        unless ($parameter->default) {
            my $equals = shift  @$parameter_tokens;
            unshift @tokens, $equals;
        }
        $parameter->tokens (\@tokens);
    } else {
        unshift @tokens, ' = ';
        $parameter = new MilHist::Template::Parameter (
            'name'    => $name,
            'tokens'  => \@tokens,
            'default' => 0);
        my $parameters = $self->{'parameters'};
        unshift @$parameters, $parameter;
    }
    return $parameter;
}

sub text ($) {
    my $self = shift;
    my @text = ($self->{'display_name'});
    my $parameters = $self->{'parameters'};
    foreach my $parameter (@$parameters) {
        push @text, $parameter->text ();
    }
    my $text = join '|', @text;
    $text =~ s/\|// if $self->{'name'} =~ /:$/; # Magic word
    $text = join '', '{{', $text, '}}';
    return $text;
}

sub DESTROY {
	  my $self = shift;
}

sub AUTOLOAD {
  	my $self = shift;
  	my $type = ref ($self) or
  		  Carp::croak "$self is not an object";
  	my $name = our $AUTOLOAD;
  	$name =~ s/.*://;
  	defined $self->{$name} or
  		  Carp::croak "Can't access '$name' field in object of class $type";
  	$self->{$name} = shift if (@ARG);
  	return $self->{$name};
}

sub new {
  	my $that = shift;
  	my $class = ref ($that) || $that;
  	%fields = ('parameters' => []);
  	my $self = { %fields, @ARG };
  	bless $self, $class;
  	$self->{'display_name'} = $self->{'name'};
  	$self->{'name'} =~ s/^\s+//;
  	return $self;
}

1;